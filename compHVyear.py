#!/usr/bin/env python
# 
# Python script to take the Ellipticity measurement
# and compute the H/V results summarised over a month
#

import os 
import argparse
from calendar import monthrange
from numpy import array, mean, std, median, percentile, unique, \
        transpose, loadtxt
from pandas import read_csv
#---------------------------------------------------------------------
def aveHV(freq_list, freq, hv):
    '''
    Estimate the average properties of the HV measurements as a 
    function of period
    '''
    hv_mean, hv_std, hv_median, hv_err_pc, T_list \
            = [], [], [], [], []
    for f in sorted(freq_list, reverse=True):
        T = 1./f # compute the period
	idx=[i for i,val in enumerate(freq) if val==f]
	idx=array(idx)
        # now do the calculations	
        hv_sub = array(hv)[idx]
	hv_mean_tmp = mean(hv_sub)
	hv_std_tmp = std(hv_sub)
	hv_median_tmp = median(hv_sub)
        q1, q2 = percentile(hv_sub,[84.1, 15.9])
	err_inf = abs(q2 - hv_median_tmp)
	err_sup = abs(q1 - hv_median_tmp)
	iqr_err = (err_inf + err_sup)/2.0
	
	hv_mean.append(hv_mean_tmp)
	hv_std.append(hv_std_tmp)
	hv_median.append(hv_median_tmp)
	hv_err_pc.append(iqr_err)
	T_list.append(T)

    return T_list, hv_mean, hv_std, hv_median, hv_err_pc

#---------------------------------------------------------------------
def flipBaz(azi):
    '''
    Flip the back azimuth values to be between 0-360 
    '''
    baz = azi
    for jj, theta in enumerate(baz):
        if theta < 0:
            baz[jj] = 180 + theta % 180
            
    return baz 

#---------------------------------------------------------------------
def angularFilter(freq, hv, azi, ang_thresh):
    '''
    Filter the data based on the user defined angular threshold
    '''
    baz = flipBaz(azi)
    if ang_thresh == None:
        return freq, hv, azi
    else:
        angMin = 180 + ang_thresh[0] % 180    
        angMax = 180 + ang_thresh[1] % 180    
        # filter the frequency and H/V values
        if angMin > angMax:
            idx = [i for i, v in enumerate(baz) if v >= angMin or v < angMax]
        else:
            idx = [i for i, v in enumerate(baz) if v >= angMin and v < angMax]

        freqF = freq[idx]
        hvF = hv[idx]
        aziF = azi[idx]

        return freqF, hvF, aziF


#---------------------------------------------------------------------
def main():
    parser = argparse.ArgumentParser(description=
                "Year summary plots of the results of H/V analysis",
                formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('--station', type=str,
                    help='station name to process', required=True)
    parser.add_argument('--years', nargs='+',
                    help='years to process', required=True )
    parser.add_argument('--code', type=str,
                    help='measurement base code', 
                    required=True)
    parser.add_argument('--locID', nargs='+' ,
                    help='location ID of the station', 
                    required=True)
    parser.add_argument('--dop', type=float,
                    help='DOP filter. Default 0.9', required=False)
    parser.add_argument('--angles', nargs=2, type=float,
                    help='Optional angular filter to compute H/V stats.\n'
                    'The input should ordered minAng max Ang.Default=0-360', 
                    required=False)
    parser.add_argument('--minObs', type=int,
                    help='Minimum number of observations in frequency range to compute \n'
                    'H/Vstats. Default is to use all available observations', 
                    required=False)
 
    # setting up the variables to plot
    args = vars(parser.parse_args())
    station = args['station']
    years = args['years']
    measurement_code = args['code']
    locID = args['locID']
    if args['dop'] is None:
        dop_thresh = 0.9
    else:
        dop_thresh = args['dop']
    day_sec = 24.*60.*60.
    
    if args['angles'] is None:
        ang_thresh = None 
    else:
        ang_thresh = args['angles']

    if args['minObs'] is None:
        minObs = 0 
    else:
        minObs = args['minObs']

 
    # variable names
    vN = ["azi", "freq", "dop", "doy", "time", "hv"]
    # loop over the years
    for kk, year in enumerate(years):
        for loc in locID:
            fnameIn = ('%s/%s_%s_%s_summary.txt' 
                    %(station, year,measurement_code, loc))
            
            if not os.path.isfile(fnameIn):
                print 'ERROR: file %s does not exist' %fnameIn
                return
           
            # use pandas to open the folder
            pF  = read_csv(fnameIn, sep=",", comment="#", names = vN)
            azi = pF.azi.values
            freq = pF.freq.values
            dop = pF.dop.values
            doy = pF.doy.values
            time = pF.time.values
            hv = pF.hv.values
            # pandas file cleanup
            del pF
             
            print '--> Processing file %s' %fnameIn
            freq_list = list(unique(freq))
            # compute the period 
            T = []
            for f in freq:
	        t=1./f #compute the period
                T.append(t)
            
            # filter the frequency and hv results based on dop
            freqD = freq[dop >= dop_thresh]
            hvD = hv[dop >= dop_thresh]
            # filter the data based on the angle of interest
            freqA, hvA, aziA = angularFilter(freqD, hvD, aziD, ang_thresh) 

            # get the statistsics for the current month 
            T_list, hv_mean, hv_std, hv_median, hv_err = aveHV(
                    freq_list, freqA, hvA)
            # save the results into 2 files based on the mean/std
            # and median/interquartile range
            out_dir = "%s/Inversion/data/ORF" % station
            if not os.path.isdir(out_dir):
                os.mkdirs(out_dir)

            if ang_thresh == None:
                angTstr = '0-360'
            else:
                angTstr = '%d-%d' %(ang_thresh[0], ang_thresh[1])
            

            output_file = (("%s/%s_%s_%s_%s_%d_%s_mean_summary.ell") 
                % (out_dir, station, measurement_code, loc, year, 
                    minObs, angTstr))
            
            with open(output_file, 'w') as outfile:
                header = 'T       H/V        sd\n'
                outfile.write(header)
                for jj,line in enumerate(T_list):
                    outfile.write('%.2f  %4.6f  %4.6f\n'
                        %(T_list[jj], hv_mean[jj], hv_std[jj]))
            outfile.close() 

            output_file = (("%s/%s_%s_%s_%s_%d_%s_median_summary.ell") 
                % (out_dir, station, measurement_code, loc, year,
                    minObs, angStr))
            
            with open(output_file, 'w') as outfile:
                header = '%-5s  %-6s  %6s\n' %('T', 'H/V', 'sd')
                outfile.write(header)
                for jj,line in enumerate(T_list):
                    outfile.write('%5.2f  %6.4f  %6.4f\n' 
                        %(T_list[jj], hv_median[jj], hv_err[jj]))

            outfile.close() 
#---------------------------------------------------------------------
if __name__=="__main__":
    main()

