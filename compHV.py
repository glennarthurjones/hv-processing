#!/usr/bin/env python
#
# Python script to take the Ellipticity measurement
# and compute the H/V results summarised over a month
#

import sys
import argparse
from os.path import basename
from calendar import monthrange
from numpy import array, arange, mean, std, median, percentile, unique, \
        transpose, loadtxt
#---------------------------------------------------------------------
def aveHV(freq_list, freq, hv, minObs, maxT):
    '''
    Estimate the average properties of the HV measurements as a 
    function of period
    '''
    # check to see if we have a period threshold and sort if it exists
    hv_mean, hv_std, hv_median, hv_err_pc, T_list \
            = [], [], [], [], []
    for f in sorted(freq_list, reverse=True):
        T = 1./f # compute the period
	idx=[i for i,val in enumerate(freq) if val==f]
	idx=array(idx)
        # check to make sure we have enough observations 
        # to compute the statistsics
        if len(idx) > minObs: 
            # now do the calculations	
            hv_sub = array(hv)[idx]
            hv_mean_tmp = mean(hv_sub)
            hv_std_tmp = std(hv_sub)
            hv_median_tmp = median(hv_sub)
            q1, q2 = percentile(hv_sub,[84.1, 15.9])
            err_inf = abs(q2 - hv_median_tmp)
    	    err_sup = abs(q1 - hv_median_tmp)
    	    iqr_err = (err_inf + err_sup)/2.0
            # check to see if we have a maximum period filter
            if not maxT:
                hv_mean.append(hv_mean_tmp)
    	        hv_std.append(hv_std_tmp)
    	        hv_median.append(hv_median_tmp)
    	        hv_err_pc.append(iqr_err)
    	        T_list.append(T)
#            elif T <= maxT:
            elif maxT[0] <= T <= maxT[1]:
                hv_mean.append(hv_mean_tmp)
    	        hv_std.append(hv_std_tmp)
    	        hv_median.append(hv_median_tmp)
    	        hv_err_pc.append(iqr_err)
    	        T_list.append(T)

    return T_list, hv_mean, hv_std, hv_median, hv_err_pc

#---------------------------------------------------------------------
def flipBaz(azi):
    '''
    Flip the back azimuth values to be between 0-360 
    '''
    baz = azi
    for jj, theta in enumerate(baz):
        if theta < 0:
            baz[jj] = 180 + theta % 180
            
    return baz 

#---------------------------------------------------------------------
def angularFilter(freq, hv, azi, ang_thresh):
    '''
    Filter the data based on the user defined angular threshold
    '''
    baz = flipBaz(azi)
    if ang_thresh == None:
        return freq, hv, azi
    else:
        if ang_thresh[0] < 0:
            angMin = 180 + ang_thresh[0] % 180    
        else:
            angMin = ang_thresh[0]

        if ang_thresh[1] < 0:
            angMax = 180 + ang_thresh[1] % 180    
        else:
            angMax = ang_thresh[1]
        # filter the frequency and H/V values
        if angMin > angMax:
            idx = [i for i, v in enumerate(baz) if v >= angMin or v < angMax]
        else:
            idx = [i for i, v in enumerate(baz) if v >= angMin and v < angMax]

        freqF = freq[idx]
        hvF = hv[idx]
        aziF = azi[idx]

        return freqF, hvF, aziF

#---------------------------------------------------------------------
def MADfilter(T, hvMean, hvStd, hvMedian, hvErr, mad_thresh):
    '''
    Filter the data based on the Median Deviation or
    Mean Deviation.  
    '''
    # Compute the mean absolute deviation of the mean data
    Xmean = mean(hvMean)
    Xmedian = median(hvMedian)

    MD_mean = abs(hvMean - Xmean)
    MD_median = abs(hvMedian - Xmedian)
    if mad_thresh == None:
        Tmedian = T
        Tmean = T
        hv_mean = hvMean
        hv_std = hvStd
        hv_median = hvMedian
        hv_err = hvErr
    else:
        idxMean = [ii for ii, v in enumerate(MD_mean) if v <= mad_thresh]
        idxMed = [ii for ii, v in enumerate(MD_median) if v <= mad_thresh]
        
        Tmean = array(T)[idxMean]
        hv_mean = array(hvMean)[idxMean]
        hv_std = array(hvStd)[idxMean]

        Tmedian = array(T)[idxMed]
        hv_median = array(hvMedian)[idxMed]
        hv_err = array(hvErr)[idxMed]
    
    return Tmean, hv_mean, hv_std, Tmedian, hv_median, hv_err

#---------------------------------------------------------------------
def main():
    '''
    Script to take the polarization results from script measure.py
    and compute the statistical H/V ratio as a fuction of period and the associated
    error
    '''

    parser = argparse.ArgumentParser(description=
                        "Compute the statistical H/V ratio for a months worth of data",
                        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--station', type=str,
                    help='station name to process', required=True)
    parser.add_argument('--year', nargs=1,
                    help='year to process', required=True )
    parser.add_argument('--months', nargs='+',
                    help='months to process', required=True)
    parser.add_argument('--code', type=str,
                    help='measurement base code', 
                    required=True)
    parser.add_argument('--locID', nargs='+' ,
                    help='location ID of the station', 
                    required=True)
    parser.add_argument('--dop', type=float,
                    help='DOP filter. Default 0.9', required=False)
    parser.add_argument('--angles', nargs=2, type=float,
                    help='Optional angular filter to compute H/V stats.\n'
                    'The input should ordered minAng max Ang.Default=0-360', 
                    required=False)
    parser.add_argument('--minObs', type=int,
                    help='Minimum number of observations in frequency range to compute \n'
                    'H/V stats. Default is to use all available observations', 
                    required=False)
    parser.add_argument('--periods', nargs=2, type=float,
                    help='Period range to compute the HV stats \n'
                    'Default is to use all available observations',
                    required=False)
    parser.add_argument('--mdThresh', type=float,
                    help='Mean/Median deviation threshold to filter the data.\n'
                    'This guarentees a smooth H/V curve.\n'
                    'Default is to use all availabe observations', 
                    required=False)
 
#    parser.add_argument('--maxT', type=float,
#                    help='Maximum period to compute the HV stats \n'
#                    'Default is to use all available observations', 
#                    required=False)

    # setting up the variables
    args = vars(parser.parse_args())
    station = args['station']
    year = args['year'][0]
    months = args['months'] 
    if len(months) > 1:
        months.sort(key=int)

    measurement_code = args['code']
    locID = args['locID']
    if args['dop'] is None:
        dop_thresh = 0.9
    else:
        dop_thresh = args['dop']

    if args['angles'] is None:
        ang_thresh = None 
    else:
        ang_thresh = args['angles']

    if args['minObs'] is None:
        minObs = 0 
    else:
        minObs = args['minObs']

    if args['mdThresh'] is None:
        mad_thresh = None
    else:
        mad_thresh = args['mdThresh']

#    if args['maxT'] is None:
#        maxT = None 
#    else:
#        maxT = args['maxT']
    if args['periods'] is None:
        Tthresh = None 
    else:
        Tthresh = args['periods']
        Tthresh.sort()

    day_sec = 24.*60.*60.
    # loop over the months
    for kk, month in enumerate(months):
        dirname = "%s/%s.%02d" %(station, year, int(month))
        for loc in locID: 
            # load the relevant summary file
            fnameIn = ('%s/%s_%s_summary.txt'
                    %(dirname, measurement_code, loc))
            try:
                azi, freq, dop, doy, time, hv = loadtxt(fnameIn,
                        delimiter=',', comments='#', unpack=True)      
            except Exception as error:
                print('WARNING: %s' %error)
                continue

            print '--> Processing file %s' %fnameIn
            freq_list = list(unique(freq))
            # compute the period 
            T = []
            T_list = []
            for f in freq:
	        t=1./f #compute the period
                T.append(t)
            
            # filter the frequency and hv results based on dop
            freqD = freq[dop >= dop_thresh]
            hvD = hv[dop >= dop_thresh]
            aziD = azi[dop >= dop_thresh]
            # filter the data based on the angle of interest
            freqA, hvA, aziA = angularFilter(freqD, hvD, aziD, ang_thresh) 

            # get the statistsics for the current month 
            T_list, hv_mean, hv_std, hv_median, hv_err = aveHV(
                    freq_list, freqA, hvA, minObs, Tthresh)
            
            # filter the data based on its deviation from the mean/median
            (T_listMean, hv_meanMean, hv_stdMean, 
                T_listMed, hv_medianMed, hv_errMed) = MADfilter(
                    T_list, hv_mean, hv_std, hv_median, hv_err, mad_thresh)

            # save the results into 2 file based on the mean/std and 
            # median/interquartile range
            if ang_thresh == None:
                angTstr = '0-360'
            else:
                angTstr = '%d-%d' %(ang_thresh[0], ang_thresh[1])
            
            output_file = ('%s/%s_%s_%s_%s.%02d_%d_%s_mean.ell' 
                    %(dirname, station,loc, measurement_code, year, int(month), 
                        minObs, angTstr))
            
            with open(output_file, 'w') as outfile:
                header = 'T       H/V        sd\n'
                outfile.write(header)
                for jj,line in enumerate(T_listMean):
                    outfile.write('%.2f  %4.6f  %4.6f\n'
                            %(T_listMean[jj], hv_meanMean[jj], hv_stdMean[jj]))
            
            output_file = ('%s/%s_%s_%s_%s.%02d_%d_%s_median.ell' 
                    %(dirname, station,loc, measurement_code, year, int(month), 
                        minObs, angTstr))
            
            
            with open(output_file, 'w') as outfile:
                header = '%-5s  %-6s  %6s\n' %('T', 'H/V', 'sd')

                outfile.write(header)
                for jj,line in enumerate(T_listMed):
                    outfile.write('%5.2f  %6.4f  %6.4f\n' 
                            %(T_listMed[jj], hv_medianMed[jj], hv_errMed[jj]))


#            savetxt(output_file, transpose([T_list, hv_mean, hv_std]), 
#                fmt=['%.2f','%4.6f','%4.6f'], delimiter=' ', header='T, H/V, sd')

#---------------------------------------------------------------------
if __name__=="__main__":
    main()

