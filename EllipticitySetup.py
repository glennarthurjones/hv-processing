#!/usr/bin/env python 
#
# Module which will help the user define parameters for the H/V
# code. For more details on how these parameters are determined please
# see lines 351-400 of polfre_2_s1_69el
#
# Written by Glenn Jones, Swansea University, 16-07-2018
import numpy as np

#==============================================================================
def steParams(npts, dt, f1, f2, nfreq):
    """
    Computes the inputs which should be used in order to get the desired
    frequencies and bounds

    Input:
        npts:       number of points in the seismic trace
        dt:         sampling interval in Hz of the seismic trace
        f1:         desired lower frequency bounds
        f2:         desired upper frequency bounds
        nfreq:      desired number of frequency bounds 

    Output:
        df:         frequency sampling interval
        f1N:        values for frequency to be > f1     
        f2N:        values for frequency to be < f2
        nfreqN:     number of frequency samples to get nfreq
        dfstepN:    actual sampling rate used based on nfreq

    """
    # getting the expected new values of f1N and f2N
    nfreqOrig = nfreq
    # get next power of 2 samples
    npts = nextpo2(npts) 
   
    fnyq = 0.5/dt
    if (f2 > fnyq): f2=fnyq
    
    # update the frequency bounds to the nearest frequency bin
    df = 1./(float(npts)*dt)
    f1N = np.ceil(f1/df)*df # use ceiling function to get new f1 in bound
    # check to make sure the new f1N is less than f1
    while f1N < f1:
        f1N = f1N + df
        
    f2N = int(f2/df)*df

    # update the frequency step
    dfstep = (f2N-f1N)/float(nfreq-1)
    # check to make sure the frequency step is at least the same size asdf
    if dfstep < df: dfstep=df 
    ndfstep = int(dfstep/df) # integer difference between old and new sampling
    dfstepN = ndfstep*df # new sampling rate
    nfreq = int((f2N-f1N)/dfstepN) + 2
    # I'm subtracting an aditional sample here so that when I use the processing
    # script I get the correct value of f2N 
    f2N = (nfreq-1)*dfstepN + f1 - dfstepN
    nfreqN = nfreq-2 # factor of 2 is to account for the modification done above 

    return df, f1N, f2N, nfreqN, dfstepN

#=============================================================================
def compParams(npts, dt, f1, f2, nfreq):
    """
    Compute frequency interval and updates to f1, f2 and nfs based
    on the fact that npts will be upsampled to the nearest power of 2

    Input:
        npts:   number of points in the seismic trace
        dt:     sampling interval in Hz of the seismic trace
        f1:     lower frequency bounds
        f2:     upper frequency bounds
        nfreq:  desired number of frequency bounds 

    Output:
        df:     natural frequency sampling interval
        f1:     update to the lower frequency bound
        f2:     update to the upper frequency bound
        nfreq:  update to the number of frequency samples
        dfstep: actual sampling rate used based on nfreq
    """

    # get next power of 2 samples
    npts = nextpo2(npts) 
   
    fnyq = 0.5/dt
    if (f2 > fnyq): f2=fnyq
    
    # update the frequency bounds to the nearest frequency bin
    df = 1./(float(npts)*dt)
    f1 = int(f1/df)*df
    f2 = int(f2/df)*df

    # update the frequency step
    dfstep = (f2-f1)/float(nfreq-1)
    # check to make sure the frequency step is at least the same size asdf
    if dfstep < df: dfstep=df 
    ndfstep = int(dfstep/df) # integer difference between old and new sampling
    dfstep = ndfstep*df # new sampling rate
    nfreq = int((f2-f1)/dfstep) + 2
    f2 = (nfreq-1)*dfstep + f1

    return df, f1, f2, nfreq, dfstep

#==============================================================================
def nextpo2(npts):
    """
    Compute the next power of 2

    Input:
        npts: number of points in the seismic trace

    Outputs:
        np2: next power of 2 samples from npts
    """
    np2 = pow(2, np.ceil(np.log(npts)/np.log(2)))
    return int(np2)

