#!/usr/bin/env python
#
# Python script to compute the theoretical ellipticity curves for a station
# using litho1.0. It is assumed that build_litho_model.py has been previously used

import glob
import sys
import os
import argparse
import math
#---------------------------------------------------------------------
def forwardModeling(station, periods, fname, order):
    cwd = os.getcwd()
    basedir = "%s/Inversion" % station
    os.chdir(basedir)
    # check to see if the litho file exists
    if not os.path.isfile(fname):
        print "--> ERROR litho file %s does not exist" % fname
        sys.exit()
    line1 =  (('sprep96  -M  %s -NMOD %d -PARR %s' + '  -R   > /dev/null') 
             % (fname, order,periods))

    os.system(line1)
    os.system('sdisp96  > sdisp96.out')
    os.system('sregn96  > sregn96.out')
    command = "sdpegn96 -R -U -ASC > ciccio"
    os.system(command)
    lines = open("SREGN.ASC","r").readlines()
    T = []
    E = []
    for i in range(1,len(lines)):
        tcalc = float(lines[i].split()[2])
        #   Get the HV value and check if it's +ve      
        HV = float(lines[i].split()[8])
        if HV > 0:
            Ecalc = math.log10(float(lines[i].split()[8]))
        else:
            Ecalc=float('NaN')
        T.append(tcalc)
        E.append(Ecalc)

    os.chdir(cwd)

    return T, E

#---------------------------------------------------------------------
def main():
    '''
    Script to take the polarization results from script measure.py
    and compute the H/V ratio as a fuction of period and the associated
    error
    '''
    parser = argparse.ArgumentParser(description=
                "Compute theoretical ellipticity (H/V) curves for a given model\n"
                "It is assumed that the model has been generated using \n"
                "build_lith_model.py and stored in the directory \n"
                "Station_name/Inversion",
                formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--station', type=str,
                    help='station name to process', required=True)
    parser.add_argument('--periods', type=str,
                    help='name of file with periods to compute', required=True)
    parser.add_argument('--fname', type=str,
                    help='Model to be used. Default is station.litho1.0.d', 
                    required=False)
    parser.add_argument('--order', type=int,
                    help='Order of models. Default is 1', required=False)


    # setting up the variables
    args = vars(parser.parse_args())
    station = args['station']
    periods = args['periods']

    if args['fname'] is None:
        fname = "%s.litho1.0.d" % station 
    else:
        fname = args['fname']
    if args['order'] is None:
        order =  1
    else:
        order = int(args['order'])

    if order < 1:
        print "WARNING: Order must be greater than 0. Setting to 1"
        order=1
    
    # perform the forward modelling
    T, E =  forwardModeling(station, periods, fname, order)

    # save the results
    fnameOut = "%s/Inversion/%s.predicted_data_order%d.txt" %(station, station, order)
    out = open(fnameOut, "w")
    for i in range(0,len(E)):
        out.write(str(T[i])+'\t'+str(round(E[i],5))+'\n')
    out.close()
#---------------------------------------------------------------------
if __name__=="__main__":
    main()

