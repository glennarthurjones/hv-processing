#!/usr/bin/env python
#
# Python script to copy the H/V summary measurements into the
# inversion directory

import sys
import os
import argparse

def main():
    '''
    Script to copy the H/V monthly summaries
    '''

    parser = argparse.ArgumentParser(description=
                        "Script to copy the H/V monthly summaries",
                        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--station', type=str,
                    help='station name to process', required=True)
    parser.add_argument('--year', nargs=1,
                    help='year to process', required=True )
    parser.add_argument('--months', nargs='+',
                    help='months to process', required=True)
    parser.add_argument('--code', type=str,
                    help='measurement base code', 
                    required=True)
    parser.add_argument('--locID', nargs='+' ,
                    help='location ID of the station', 
                    required=True)
    parser.add_argument('--angles', nargs=2, type=float,
                    help='Optional angular filter used to compute H/V stats.\n'
                    'Default=0-360', 
                    required=False)
    parser.add_argument('--minObs', type=int,
                    help='Minimum number of observations used to compute \n'  
                    'H/Vstats.Default is to use all available observations', 
                    required=False)

    # setting up variables
    args = vars(parser.parse_args())
    station = args['station']
    year = args['year'][0]
    months = args['months'] 
    if len(months) > 1:
        months.sort(key=int)

    measurement_code = args['code']
    locID = args['locID']
    
    if args['angles'] is None:
        ang_thresh = None 
    else:
        ang_thresh = args['angles']

    if args['minObs'] is None:
        minObs = 0 
    else:
        minObs = args['minObs']
   
    if ang_thresh == None:
       angTstr = '0-360'
    else:
        angTstr = '%d-%d' %(ang_thresh[0], ang_thresh[1])
            
    for kk, month in enumerate(months):
        dirname = "%s/%s.%02d" %(station, year, int(month))
        for loc in locID: 
            # check to see if the file exists
            fnameMean = ('%s/%s_%s_%s_%s.%02d_%d_%s_mean.ell' 
                    %(dirname, station, loc, measurement_code, year, int(month), 
                        minObs, angTstr))
            if os.path.isfile(fnameMean):
                # check to see if the inversion folder exists
                out_dir = "%s/Inversion/data/ORF" % station
                if not os.path.isdir(out_dir):
                    os.makedirs(out_dir)

                # copy the files from A to B
                copy_str = "cp %s %s/." % (fnameMean, out_dir)

            # check to see if the file exists
            fnameMedian = ('%s/%s_%s_%s_%s.%02d_%d_%s_median.ell' 
                    %(dirname, station, loc, measurement_code, year, int(month), 
                        minObs, angTstr))
            if os.path.isfile(fnameMedian):
                # check to see if the inversion folder exists
                out_dir = "%s/Inversion/data/ORF" % station
                if not os.path.isdir(out_dir):
                    os.makedirs(out_dir)

                # copy the files from A to B
                copy_str = "cp %s %s/." % (fnameMedian, out_dir)
                os.system(copy_str)
#---------------------------------------------------------------------
if __name__=="__main__":
    main()

