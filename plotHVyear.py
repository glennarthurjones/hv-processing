#!/usr/bin/env python
# 
# Python script to generate statistical measures of H/V  
import os
import argparse
import datetime
import numpy as np
import matplotlib.pyplot as plt
from calendar import monthrange, month_name, datetime
from matplotlib import colors
from matplotlib import gridspec
from pandas import read_csv

#---------------------------------------------------------------------
def convertDOYmonth(doy, year):
    '''
    Convert a list of doy of year values into months
    '''
    months = []
    for d in doy:
        date = "%s+%d" %(year, d)
        m = datetime.datetime.strptime(date, "%Y+%j")
        months.append(m.month)

    return months
#---------------------------------------------------------------------
def flipBaz(azi):
    '''
    Flip the back azimuth values to be between 0-360 
    '''
    baz = azi
    for jj, theta in enumerate(baz):
        if theta < 0:
            baz[jj] = 180 + theta % 180
            
    return baz 

#---------------------------------------------------------------------
def aveHV(freq_list, freq, hv, minObs):
    '''
    Estimate the average properties of the HV measurements as a 
    function of period
    '''
    hv_mean, hv_std, hv_median, hv_err_pc, T_list, Nobs\
            = [], [], [], [], [], []
    hv = np.array(hv)
    deltaF = float(freq_list[1] - freq_list[0])
#    for f in sorted(freq_list, reverse=True):
    fl = sorted(freq_list, reverse=True)
    for k in range(0,len(fl)-1):
#        T = 1./f # compute the period
#        fbinMin = f-deltaF/2
#        fbinMax = f+deltaF/2
        fbinMin = fl[k+1]
        fbinMax = fl[k]
        T = "%.2f - %.2f" %(1./fbinMax, 1./fbinMin)
	idx=[i for i,val in enumerate(freq) if val >= fbinMin and val <fbinMax]
        if idx:
	    idx=np.array(idx)
        else:
            continue
        
        if len(idx) > minObs:
            # now do the calculations	
            hv_sub = hv[idx]
	    hv_mean_tmp = np.mean(hv_sub)
	    hv_std_tmp = np.std(hv_sub)
	    hv_median_tmp = np.median(hv_sub)
            q1, q2 = np.percentile(hv_sub,[84.1, 15.9])
	    err_inf = abs(q2 - hv_median_tmp)
	    err_sup = abs(q1 - hv_median_tmp)
	    iqr_err = (err_inf + err_sup)/2.0
	    
	    hv_mean.append(hv_mean_tmp)
	    hv_std.append(hv_std_tmp)
	    hv_median.append(hv_median_tmp)
	    hv_err_pc.append(iqr_err)
	    T_list.append(T)
            Nobs.append(len(idx))

    return T_list, hv_mean, hv_std, hv_median, hv_err_pc, Nobs

#---------------------------------------------------------------------
def getAziStat(freq_list, freq, hv, azi, minObs):
    '''
    Take azimuthal bins of 90 degrees and compute the stats 
    in each. The stats will be computed for 50% overlap between bins
    '''
    binStats={}
    wLen = 45.
    bins = np.arange(0, 360, wLen/2)
    ii = len(bins)
    aziM = flipBaz(azi)
    # loop over the bins and compute the stats
    for q, ang in enumerate(bins):
        angMin = np.mod(ang-wLen/2, 360)
        angMax= np.mod(ang+wLen/2, 360)
        # filter the frequency and 
        if angMin > angMax:
            idx = [i for i, v in enumerate(aziM) if v >= angMin or v < angMax]
        else:
            idx = [i for i, v in enumerate(aziM) if v >= angMin and v < angMax]
        # make sure we dont have an empty list
        if idx:
	    idx=np.array(idx)
        else:
            continue

        freqF = freq[idx]
        hvF = hv[idx]
        # compute the average results
        tt, hv_mean, hv_std, hv_median, hv_err, nObs = aveHV(
                freq_list, freqF, hvF, minObs)

        for k,t in enumerate(tt):
            tID = "%s" % t
            angID = "%s" % ang
#            if q==0:
            if not binStats.has_key(tID):
                binStats[tID]={}

            binStats[tID][angID] = [hv_median[k], hv_err[k], nObs[k]]

    return binStats, bins

#---------------------------------------------------------------------
def MinMaxObs(obs):
    '''
    Get the minimum and maximum number of observations to build and
    appropriate colour scale for the plot
    '''
    minObs = 1e10
    maxObs = -1e10
    for x in obs.keys():
        for y in obs[x].keys():
            dta = obs[x][y][:,2]
            # update min/max and remove any nans 
            if np.min(dta[~np.isnan(dta)]) < minObs:
                minObs = np.min(dta[~np.isnan(dta)])
            if np.max(dta[~np.isnan(dta)]) > maxObs:
                maxObs = np.max(dta[~np.isnan(dta)])

    return minObs, maxObs

#---------------------------------------------------------------------
def main():
    '''
    Script to plot a yearly summary of the H/V analysis as a function
    of azimuth and period
    '''
    parser = argparse.ArgumentParser(description=
                "Year summary of th H/V values as a function of noise souce azimuth\n"
                "Must have performed monthly and yearly summary prior \n"
                "to running command",
                formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('--station', type=str,
                    help='station name to process', required=True)
    parser.add_argument('--years', nargs='+',
                    help='years to process', required=True )
    parser.add_argument('--code', type=str,
                    help='measurement base code', 
                    required=True)
    parser.add_argument('--locID', nargs='+' ,
                    help='location ID of the station', 
                    required=True)
    parser.add_argument('--dop', type=float,
                    help='DOP filter. Default 0.9', required=False)
    parser.add_argument('--minObs', type=int,
                    help='Minimum number of observations in frequency range to compute \n'
                    'H/Vstats. Default is to use all available observations', 
                    required=False)

    # setting up the variables to plot
    args = vars(parser.parse_args())
    station = args['station']
    years = args['years']
    measurement_code = args['code']
    locID = args['locID']
    if args['dop'] is None:
        dop_thresh = 0.9
    else:
        dop_thresh = args['dop']
    if args['minObs'] is None:
        minObs = 0 
    else:
        minObs = args['minObs']

    day_sec = 24.*60.*60.
    # variable names
    vN = ["azi", "freq", "dop", "doy", "time", "hv"]
    # loop over the years
    c=0
    for kk, year in enumerate(years):
        for loc in locID:
            c+=1
            fnameIn = ('%s/%s_%s_%s_summary.txt' 
                    %(station, year,measurement_code, loc))
            
            if not os.path.isfile(fnameIn):
                print 'ERROR: file %s does not exist' %fnameIn
                return
           
            # use pandas to open the folder
            pF  = read_csv(fnameIn, sep=",", comment="#", names = vN)
            azi = pF.azi.values
            freq = pF.freq.values
            dop = pF.dop.values
            doy = pF.doy.values
            time = pF.time.values
            hv = pF.hv.values
            # pandas file cleanup
            del pF
            # compute the frequency list
            freq_list = list(np.unique(freq))
            doyF = doy[dop >= dop_thresh]
            freqF = freq[dop >= dop_thresh]
            hvF = hv[dop >= dop_thresh]
            baz = np.array(azi[dop >= dop_thresh])
            baz = flipBaz(baz) # convert between 0 and 360
            # compute the period 
            T=1./freqF

            # filter the data based on the period
            doyFT = doyF[T < 9]
            freqFT = freqF[T < 9]
            hvFT = hvF[T < 9]
            bazT = baz[T < 9]
            
            # we want to get the average HV values in bins of 0.1 to 0.5 Hz
            fbins = np.linspace(0.1,0.5,5)
            # generate an array of month in which the data were recorded
            months = convertDOYmonth(doyFT, year)
            months = np.array(months)
            dtaMonth = {}
            for qq, month in enumerate(range(min(months), max(months)+1)):
                freqFTM = freqFT[months==month] 
                hvFTM = hvFT[months==month]
                aziFTM = bazT[months==month]
                # compute the statistics as a function of azimuth
                HVbinStats, bins = getAziStat(fbins, freqFTM, hvFTM, aziFTM,
                        minObs)
                # store the results as a dictionary with a numpy array for the plot
                for k, key in enumerate(HVbinStats.keys()):
                    if qq==0:
                        dtaMonth[key]={}
                    # loop over the angular bins
                    for bb in bins:
                        b = "%s" %bb
                        if qq==0:
                            # initialise the matrix to store the information
                            dtaMonth[key][b] = np.zeros((12,3))
                        # check to see if the key exists and fill the matrix 
                        if HVbinStats[key].has_key(b):
                            dtaMonth[key][b][qq,:] = np.array(HVbinStats[key][b])
                        else:
                            dtaMonth[key][b][qq,:] = np.array([np.nan, np.nan, np.nan])

            #==================================================================
            # Plot the results
            fig = plt.figure(c, figsize=(16,9))
            rows=2
            cols=2
            gs = gridspec.GridSpec(2,5)
            plt.rcParams['xtick.labelsize']=14
            plt.rcParams['ytick.labelsize']=14
            pMarkers = ["o","v","^", "<", ">", "s", "p", "|", 
                    "*", "+", "x", "d", "H", ".", "1", "2"]
#            pMarkers = ["*", "x", "+", ">","o","v","^", "s"] 
            minO, maxO = MinMaxObs(dtaMonth)
            normCol = colors.LogNorm(vmin=minO, vmax=maxO)
            
            for q, key in enumerate(dtaMonth.keys()):
                row = (q // rows)
                col = q % cols
                col = col*2
                Tid = key
#                ax = plt.subplot("22%d" %(q+1))
                ax = plt.subplot(gs[row, col:col+2])
                for jj,bb in enumerate(bins):
                    b = "%s" %bb
                    dta = dtaMonth[Tid][b]
                    pcs=ax.scatter(np.arange(1,13),dta[:,0], c=dta[:,2], 
                            marker=pMarkers[jj], edgecolors=None, s=100,
                            label=("%s$^\circ$" %b), norm=normCol, 
                            cmap='viridis', alpha=0.8)
           
                    # add the bounding box to the current iteration 
                    #if (q+1) == len(dtaMonth.keys()):
                    if (q+1) == 2: 
                        ax.legend(bbox_to_anchor=(1.02, 1), fontsize=16, 
                            scatterpoints=1,loc='upper left', ncol=2,
                            borderaxespad=0.,framealpha=0.5, fancybox=True)

                # set up axes and labels
                plt.title("Period %s" % key, size=20)
                plt.xlabel("Month", fontsize=18)
                plt.ylabel("log(H/V)", fontsize=18)
                plt.ylim(-0.5,0.5)
                xTicks = np.arange(0,13,2)
                xTickMark = ["%s" % x for x in xTicks]
                plt.xlim(0,12.2)
                plt.grid()
            # colorbar setup
            cax = fig.add_axes([0.8, 0.1, 0.02, 0.345])
            nearest10= np.floor(maxO/(np.log10(maxO)))
            maxVal = nearest10*10**np.floor(np.log10(maxO))
            cTicks = [10**x  for x in range(0,int(np.floor(np.log10(maxO)+1)))]
            cbar = fig.colorbar(pcs, cax=cax, ticks=cTicks)
            cbar.set_label('Number of observations',fontsize=18)
            # titles and plot adjustments
            fig.suptitle("%s %s DOP = %s Obs. > %s" 
                    %(station, year, dop_thresh, minObs), size=22)
            plt.subplots_adjust(right=0.95,left=0.08, wspace=0.5, top=0.9,hspace=0.3)
        plt.show()
#---------------------------------------------------------------------
if __name__=="__main__":
    main()




