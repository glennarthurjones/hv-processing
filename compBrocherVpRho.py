#!/usr/bin/env python 
#

import sys
import numpy as np
'''
Script to compute Vp and density given a Vs using the Brocher equations [1]

References:
    1: Brocher T. M 2005, Empirical relations between elastic wavespeeds
    and density in the Earth's crust. Bull. seis. Soc. Am., 95, 2081-2092
'''
#---------------------------------------------------------------------
def compVp(Vs):
    '''
    Compute Vp using the Brocher 2005 equation 9
    '''

    Vp = 0.9409 + 2.0947*Vs - 0.8206*(Vs**2) + 0.2683*(Vs**3) - 0.0251*(Vs**4)

    return Vp

#---------------------------------------------------------------------
def compRho(Vp):
    '''
    Compute the density using the Brocher 2005 equation 1
    '''

    R = (1.6612*Vp - 0.4721*(Vp**2) + 0.0671*(Vp**3) - 0.0043*(Vp**4) 
            + 0.000106*(Vp**5))

    return R

#---------------------------------------------------------------------
Vs = raw_input("Please input a S-wave velocity (Vs) in km/s: ")
try: 
    Vs = float(Vs)
except ValueError:
    print "Vs is not a number: Please input a number"
    sys.exit()

Vp = compVp(Vs)
R = compRho(Vp)

print "************************************************************************"
print (" Vp = %.4f (km/s), Vs = %.4f (km/s), Density = %.4f" %
        (Vp, Vs, R))
print "************************************************************************"
print ""
