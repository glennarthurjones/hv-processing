#!/usr/bin/env :
# load the postprocessed data to make sure everything is ok
import numpy as np
from os import listdir
from obspy.core import read, UTCDateTime
from tracePreProc import check_and_phase_shift, make_same_length
import matplotlib.pyplot as plt
from scipy.fftpack import fft

deltaT = 0.5

# load the the pre-processed data
st = read('DY2G/2012.03/14/processed/DK.DY2G.02.BH*_chunk_9sps2.sac')
st.sort()
st.merge()

# plot the results
plt.rcParams['xtick.labelsize']=16
plt.rcParams['ytick.labelsize']=16
ftitle = '%s.%s.%s %s' %(st[0].stats.network, st[0].stats.station,
	st[0].stats.channel, st[0].stats.starttime)

plt.figure(1, figsize=(11.69, 8.27))
for ii in range(0,3):
	plt.subplot(3,1,ii+1)
	if ii==0:
		plt.title(ftitle,size=18)
	tr = st[ii]
	t = np.arange(0, tr.stats.npts / tr.stats.sampling_rate, tr.stats.delta)

	plt.plot(t, tr, 'r', label='DY2G',linewidth=2)
	plt.grid()
	plt.ylabel('Displacement (m)', size=16)
	plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

plt.legend()
plt.xlabel('Time (s)',size=16)
plt.subplots_adjust(hspace=0.3)
plt.show()

# ========================
# plot the spectrum of the data
plt.figure(2, figsize=(11.69, 8.27))
for ii in range(0,3):
	plt.subplot(3,1,ii+1)
	if ii==0:
		plt.title(ftitle,size=18)
	tr = st[ii]
	t = np.arange(0, tr.stats.npts / tr.stats.sampling_rate, tr.stats.delta)
	freq = np.linspace(0.0, 1.0/(2.0*tr.stats.delta), tr.stats.npts/2)
	trFFT = fft(tr)
	plt.plot(freq, 2.0*tr.stats.npts*np.abs(trFFT[:tr.stats.npts//2]),'b',
		label='DY2G', alpha = 0.7, linewidth=2)

	plt.grid()
	plt.ylabel('Amplitude',size=16)
	plt.xlim(0, 0.5)	
	ax = plt.gca()
	ax.set_yscale('log')
	ax.set_xscale('log')
	
plt.legend()
plt.xlabel('Frequency (Hz)',size=16)
plt.subplots_adjust(hspace=0.3)
plt.show()
