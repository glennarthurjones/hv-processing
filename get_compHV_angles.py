#!/usr/bin/env python 
# python script to compute the statitsics associated with H/V
# measurements from measure_HV.py. The output can be used to 
# define the distribution for compHV.py
import glob
import numpy as np
import argparse
from calendar import month_name
from scipy.signal import argrelextrema
from scipy.stats import gaussian_kde
#---------------------------------------------------------------------
def flipBaz(azi):
    '''
    Flip the back azimuth values to be between 0-360 
    '''
    baz = azi
    for jj, theta in enumerate(baz):
        if theta < 0:
            baz[jj] = 180 + theta % 180
            
    return baz 

#---------------------------------------------------------------------
def makeRadVect(azi):
    '''
    Generate a radial vector which can be used to compute the 
    statistics of the data. I use Eulers formula to generate my
    radius vector
    '''
    # convert data from degrees to radians
    aziR = np.deg2rad(azi)
    r = np.exp(1j*aziR)

    return r

#---------------------------------------------------------------------
def isiterable(p_object):
    '''
    Check to see if variable is iteratable
    '''
    try:
        it = iter(p_object)
    except TypeError:
        return False
    return True 

#---------------------------------------------------------------------
def buildGK(azi, deltaA):
    '''
    Build the Gaussian Kernel with wrap around angular boundaries
    This is a naiive implementation
    '''
    # Wrap around last 5 sample bins
    nb = 5 
    # get the vales which are between 0 and nb*deltaA
    aziWrapE = azi[azi < nb*deltaA] + 360
    aziWrapS = azi[azi > 360-nb*deltaA] - 360
    aziK = np.hstack([aziWrapS, azi, aziWrapE])
    kernel = gaussian_kde(aziK, bw_method='silverman')

    return kernel

#---------------------------------------------------------------------
def heightF(data, d_min, d_max, perc=0.5):
    '''
    Remove the min and max values below a given percentage of the 
    signal height
    '''

    # normalise the signal
    dataN = (data - min(data)) / (max(data) - min(data))
    idx = np.where(dataN >= perc)[0]
    si = set(idx)
    # compute the maxima
    maxF = []
    for ii,x in enumerate(d_max):
        if x in si:
            maxF.append(x)

    # sort the maxima indices based on their values in
    s = np.argsort(dataN[maxF])[::-1]
    maxF = [maxF[j]  for j in s]

    # compute the minima
    minF = []
    for ii,x in enumerate(d_min):
        if x in si:
            minF.append(x)

    # sort the maxima indices based on their values in
    s = np.argsort(dataN[minF])[::-1]
    minF = [minF[j]  for j in s]

    return minF, maxF
    
#---------------------------------------------------------------------
def getBandwidth(data, bins):
    '''
    Compute the badwidth around the maxima of the data. The bandwidth
    is defined as the nearest local minima to the maxima or 50% of the
    maximum signal
    '''
    
    # get the local maxima and minima of the data
    d_max = argrelextrema(data, np.greater, order=3)[0]
    d_min = argrelextrema(data, np.less, order=3)[0]

    # in order to make sure we can account for peaks close to 0 degrees
    # the bins and data are padded with 45 degrees worth of data 
    deltaA = bins[1]-bins[0]
    nb = int(np.floor(45./deltaA))+1
    dataWrapE =  data[-nb:-1]
    binsWrapE =  bins[-nb:-1] - 360
    dataWrapP = data[1:nb]
    binsWrapP= 360 + bins[1:nb]
    data = np.hstack([dataWrapE, data, dataWrapP]) 
    bins = np.hstack([binsWrapE, bins, binsWrapP]) 

    # add the value of nb to each of the maxima and minima to 
    # make sure we have the correct index
    d_max = d_max + (nb-1)
    d_min = d_min + (nb-1)

    # filter the maxima based on 50% of the peak max
    minF, maxF = heightF(data, d_min, d_max)

#    HM = (data.max() - data.min())/2
    HM = data.max()/2
    # get the values of the data above 50% of the max
    idx = np.where(data >= HM)[0]
    # now we want to find the bound around the maxima which are within 
    # 50% of the peak max
    FWHM = [] 
    nbm = []
    nam = []
    # check to see if the object is iterable
    if isiterable(maxF):
        for ii, pos_max in enumerate(maxF):
            # check to make sure the maxima is not within the bounds 
            # of a previous observation
            if ii > 0:
                for jj, v in enumerate(FWHM):
                    lb = FWHM[jj][0]
                    ub = FWHM[jj][1]
                    if lb <= bins[pos_max] <= ub:
                        continue
            # find the 1st points which fall above/below 50% 
#            naboveC = min(np.where(data[pos_max:-1] < HM)[0])
            nac = np.where(data[pos_max:-1] < HM)[0]
            if nac.tolist():
                naboveC = min(nac)
            else:
                naboveC = []
#            nbelowC = max(np.where(data[0:pos_max] < HM)[0])
            nbc = np.where(data[0:pos_max] < HM)[0]
            if nbc.tolist():
                nbelowC = max(nbc)
            else:
                nbelowC = []
            # define the minima above/below the max
            if minF:
                mF = np.array(minF)
                belowA = mF[mF < pos_max]
                aboveA = mF[mF > pos_max]- pos_max
            else:
                belowA=np.array([])
                aboveA=np.array([])
            # check to see if the array is empty
#            if nbelowC.any() and belowA.any():
            if nbelowC and belowA.size:
                nbelow = np.max(np.hstack([nbelowC, belowA]))
                # check to see if nbelow equals a minima
                if np.in1d(nbelow,belowA):
                    nbm = True 

            elif nbelowC:
                nbelow = np.max(np.array(nbelowC))
            elif belowA.size:
                nbelow = np.max(np.array(belowA))
                nbm = True 
            else:
               continue 
           
#            if naboveC.any() and aboveA.any():
            if naboveC and aboveA.size:
                nabove = np.min(np.hstack([naboveC, aboveA]))
                if np.in1d(nabove,aboveA):
                    nam=True
            elif naboveC:
                nabove = np.min(np.array(naboveC))
            elif aboveA.size:
                nabove = np.min(np.array(aboveA))
                nam = True
            else:
               continue 
    
            # check to see if both values are below the half max - 
            # checking to make sure we compute the angular window 
            # based on the same amplitdudes
            if nam and nbm:
                # find out which has the largest amplitude 
                dtaA = data[pos_max + nabove]
                dtaB = data[nbelow]
                # find the maximum amplitude value
                if dtaA > dtaB:
                    nbelow = max(np.where(data[0:pos_max] < dtaA)[0])
                else:
                    nabove = min(np.where(data[pos_max:-1] < dtaB)[0])
            elif nam:
                dtaA = data[pos_max + nabove]
                nbelow = max(np.where(data[0:pos_max] < dtaA)[0])
            elif nbm:
                dtaB = data[nbelow]
                nabove = min(np.where(data[pos_max:-1] < dtaB)[0])

            binAbove = bins[pos_max+nabove]
            binBelow = bins[nbelow]
            FWHM.append([binBelow, binAbove, bins[pos_max]])
            # generate the characteristic curves to determine the
            # angular bounds
#        naboveC = (np.abs(data[pos_max:-1] - HM))
#        nbelowC = (np.abs(data[0:pos_max] - HM))
#        # compute the local minima in these curves and select the smallest
#        # value
#        nabove = argrelextrema(naboveC, np.less, mode='wrap')[0].min()
#        nbelow =  argrelextrema(nbelowC, np.less,mode='wrap')[0].max()
#        nbelow = (np.abs(data[0:pos_max] - HM)).argmin() old
        # get the angles which correspond to these values
#            binAbove = bins[pos_max+nabove]
#            binBelow = bins[nbelow]
#            FWHM.append([binBelow, binAbove, bins[pos_max]])
    else:

        nac = np.where(data[maxF:-1] < HM)[0]
        if nac.tolist():
            naboveC = min(nac)
        else:
            naboveC = []
        nbc = np.where(data[0:maxF] < HM)[0]
        if nbc.tolist():
            nbelowC = max(nbc)
        else:
            nbelowC = []
 
#        naboveC = min(np.where(data[maxF:-1] < HM)[0])
#        nbelowC = max(np.where(data[0:maxF] < HM)[0])
        # define the minima above/below the max
        if minF:
            mF = np.array(minF)
            belowA = mF[mF < pos_max]
            aboveA = mF[mF > pos_max]- pos_max
        else:
            belowA=np.array([])
            aboveA=np.array([])

#        belowA = max(np.where(minF < maxF))
#        aboveA = min(np.where(minF > maxF))
        # check to see if the array is empty
        if nbelowC.any() and belowA.any():
            nbelow = np.max(np.hstack([nbelowC, belowA]))
            # check to see if nbelow equals a minima
            if np.in1d(nbelow=belowA):
                nbm = True 

        elif nbelowC.any():
            nbelow = np.max(np.array(nbelowC))
        elif belowA.any():
            nbelow = np.max(np.array(belowA))
            nbm = True
           
        if naboveC.any() and aboveA.any():
            nabove = np.min(np.hstack([naboveC, aboveA]))
            if np.in1d(nabove,aboveA):
                nam=True
        elif nbelowC.any():
            nabove = np.min(np.array(naboveC))
        elif aboveA.any():
            nabove = np.min(np.array(aboveA))
            nam = True
#        nbelow = max([nbelowC, belowA])
#        nabove = min([naboveC, aboveA])
#        binAbove = bins[maxF+nabove]
#        binBelow = bins[nbelow]
#        FWHM.append([binBelow, binAbove, bins[maxF]])


        # check to see if both values are below the half max - checking to 
        # make sure we compute the angular window based on the same amplitdudes
        if nam and nbm:
            # find out which has the largest amplitude 
            dtaA = data[pos_max + nabove]
            dtaB = data[nbelow]
            # find the maximum amplitude value
            if dtaA > dtaB:
                nbelow = max(np.where(data[0:maxF] < dtaA)[0])
            else:
                nabove = min(np.where(data[maxF:-1] < dtaB)[0])
        elif nam:
            dtaA = data[maxF + nabove]
            nbelow = max(np.where(data[0:maxF] < dtaA)[0])
        elif nbm:
            dtaB = data[nbelow]
            nabove = min(np.where(data[maxF:-1] < dtaB)[0])

        binAbove = bins[maxF+nabove]
        binBelow = bins[nbelow]
        FWHM.append([binBelow, binAbove, bins[maxF]])

    return FWHM

#---------------------------------------------------------------------
def main():
    ''' 
    Main script to get the direction and statistics of H/V singnals 
    '''
    parser = argparse.ArgumentParser(description=
                    "Compute the peak and angular width at half max of the \n" 
                    "polarisation measurements from measure_HV.py.The output \n"
                    "can be used to define the angular distribution for compHV.py\n"
                    "\n"
                    "The computation is done by fitting a Gaussian kernel-density (KDE)\n"
                    "to the data before computing the local minima and maxima.\n"
                    "The width of the angular distribution as the width at half max\n"
                    "around the current local maxima. The local maxima must be greater\n"
                    "than a predefined threshold\n"
                    "\n"
                    "The output is txt file with the peak azimuth and the width of the\n"
                    "angular bin. The output is stored as: code_%loc_summary_angles.txt",
                        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--station', type=str,
                    help='station name to process', required=True)
    parser.add_argument('--year', nargs=1,
                    help='year to process', required=True )
    parser.add_argument('--months', nargs='+',
                    help='months to process', required=True)
    parser.add_argument('--code', type=str,
                    help='measurement base code', 
                    required=True)
    parser.add_argument('--locID', nargs='+' ,
                    help='location ID of the station', 
                    required=True)
    parser.add_argument('--dop', type=float,
                    help='DOP filter. Default 0.9', required=False)
    parser.add_argument('--deltaAng', type=float,
                    help='Angle interval to compute KDE. Default 5', required=False)


    # initialise the input aruments
    args = vars(parser.parse_args())
    station = args['station']
    year = args['year'][0]
    months = args['months'] 
    if len(months) > 1:
        months.sort(key=int)

    measurement_code = args['code']
    locID = args['locID']
    if args['dop'] is None:
        dop_thresh = 0.9
    else:
        dop_thresh = args['dop']

    if args['deltaAng'] is None:
        deltaA = 5
    else:
        deltaA= args['deltaAng']
    
    for kk, month in enumerate(months):
        dirname = "%s/%s.%02d" %(station, year, int(month))
        for loc in locID: 
            # load the relevant summary file
            fnameIn = ('%s/%s_%s_summary.txt'
                    %(dirname, measurement_code, loc))
            try:
                azi, freq, dop, doy, time, hv = np.loadtxt(fnameIn,
                        delimiter=',', comments='#', unpack=True)      
            except Exception as error:
                print('WARNING: %s' %error)
                continue

            freq_list = list(np.unique(freq))
            # compute the period 
            T = []
            for f in freq:
	        t=1./f #compute the period
                T.append(t)
 
            # filter the results based on the DOP threshold
            # convert T, time and hv into numpy arrays
            T = np.array(T)
            baz = np.array(azi[dop >= dop_thresh]) 
            baz = flipBaz(baz)
            freqD = freq[dop >= dop_thresh]
            # fit the Gaussian kernel to the back azimuth data
            kernel = buildGK(baz, deltaA)
#            kernel = gaussian_kde(baz, bw_method='silverman')
            # generate the point we wish to compute the kernel at
            hBins = np.arange(0,360+deltaA, deltaA)
            k_all = kernel(hBins)
            # get the values of the kernel above 50% of the max
#            idx = np.where(k_all >= np.max(k_all)/2)

            # get the local maxima and minima of the data
#            k_max = argrelextrema(k_all, np.greater)
#            k_min = argrelextrema(k_all, np.less)
            # now we need to check bandwidth around maxima
            bStat = getBandwidth(k_all, hBins)

            # Print the output to a file as csv with the max, min etc
            fnameOut = ('%s/%s_%s_summary_angles.txt'
                    %(dirname, measurement_code, loc))
            with open(fnameOut, 'w') as outfile:
                outfile.write("# Processed data %s\n" % fnameIn)
                header = ('%-9s, %-14s, %-14s\n' %('Max angle', 
                        'Min Half Width', 'Max Half Width'))
                outfile.write(header)
                for jj, ss in enumerate(bStat):
                    outfile.write('%9.2f, %14.2f, %14.2f\n' 
                            %(ss[2], ss[0], ss[1]))
            
            outfile.close()
        print "--> Finished processing month %s" % month
#---------------------------------------------------------------------
if __name__=="__main__":
    main()

