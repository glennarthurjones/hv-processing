#!/usr/bin/env python 
# Python script to plot the H/V resuls for DY2 station for the 14th, 20th 
# and 25th of March and compare the back-azimuth to the location of the 3 EQ
import os 
import sys 
import glob
import numpy as np
import matplotlib.pyplot as plt
import datetime
from mpl_toolkits.basemap import Basemap, addcyclic, cm
from obspy import read_inventory, read
from obspy.geodetics.base import gps2dist_azimuth
from obspy.imaging.beachball import beach
from matplotlib import colors
from scipy.ndimage.filters import minimum_filter, maximum_filter
from netCDF4 import Dataset as NetCDFFile
#---------------------------------------------------------------------
# get the min-max values for the sea-pressure levels
def extrema(mat,mode='wrap',window=10):
    """find the indices of local extrema (min and max)
        in the input array."""
    mn = minimum_filter(mat, size=window, mode=mode)
    mx = maximum_filter(mat, size=window, mode=mode)
    # (mat == mx) true if pixel is equal to the local max
    # (mat == mn) true if pixel is equal to the local in
    # Return the indices of the maxima, minima
    return np.nonzero(mat == mn), np.nonzero(mat == mx)

#---------------------------------------------------------------------
# convert between cartesian and polar coordinates
def cart2pol(x, y):
    ''' convert between cartesian to polar coordinates'''
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def pol2cart(rho, phi):
    ''' convert between polar to catresian coordinates'''
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)

#---------------------------------------------------------------------
# compute the lat and lon given a bearing and distance 
def getLatLon(lat0, lon0, theta, d):
    ''' compute the lat and lon from a point given a bearing and azimuth

        Reference: http://www.movable-type.co.uk/scripts/latlong.html
    '''
    R = 6378.1 #Radius of the Earth
    # convert lat0 and lon0 to radian
    phi1 = np.deg2rad(lat0)
    lambda1 = np.deg2rad(lon0)
    theta = np.deg2rad(theta)
    delta = d/R

    # compute the latitude
    phi2 = np.arcsin( np.sin(phi1) * np.cos(delta) + 
            np.cos(phi1) * np.sin(delta) * np.cos(theta))
    
    a = np.sin(theta) * np.sin(delta) * np.cos(phi1)
    b = np.cos(delta) - np.sin(phi1) * np.sin(phi2)

    lambda2 = lambda1 + np.arctan2(a, b)

    lat2 = np.rad2deg(phi2)
    lon2 = np.rad2deg(lambda2)

    return lat2, lon2

#---------------------------------------------------------------------
# Define some variables and read the data
station = "DY2G"
year = "2012"
month = "03"
days = [14,20, 25]
measurement_code="pol_testEQ"
dop_thresh=0.9
day_sec = 24*60*60.

#EQlats = np.array([-35.2, 16.47, 40.84])
#EQlons = np.array([-72.13, -98.37, 145.02])
#EQtimes = ["22:37:06", "18:02:47", "09:08:36"]
EQlats = np.array([40.84, 16.47,-35.2])
EQlons = np.array([145.02, -98.37,-72.13])
EQtimes = ["09:08:36","18:02:47","22:37:06"]
EQdepth = [19.2, 19.4,28.2]
# info from CMT
EQmag = [7.,7.5,7.1] # surface wave mag
MT = np.array([[-3.500, 3.110, 0.384, -0.044, 0.566, -0.127],
        [0.679, -0.564, -0.115, 1.740, -0.659, 0.242],
        [3.920, -0.038, -2.880, 0.415, -5.190, -0.474]])
for kk, day in enumerate(days):
    baseDir = "%s/%s.%s/%02d" %(station, year, month, day)
    fname= "%s/processed/*%s*" %(baseDir, measurement_code)
    
    filename = glob.glob(fname)
    # output variables 
    azi, freq, freq_list, dop, time, hv =[],[],[],[],[],[]
    if not filename:
        continue
    
    for fl in filename:
        flB = os.path.basename(fl)
        locID= flB.split('.')[4]
        with open(fl) as fp:
            for line in fp:
                if line.split()[0]== year:
		    pass
		else:
		    azi_tmp = float(line.split()[0])
		    freq_tmp = float(line.split()[1])
		    dop_tmp = float(line.split()[2])
		    time_tmp = float(line.split()[3])
		    hv_tmp = np.log10(float(line.split()[5]))
					
		    # check to make sure the results are within our DOP
		    if dop_tmp >= dop_thresh:
		        azi.append(azi_tmp)
			freq.append(freq_tmp)
			dop.append(dop_tmp)
			time.append(time_tmp)
			hv.append(hv_tmp)

			# check to see if there are unique frequency values
			if freq_tmp not in freq_list:
			    freq_list.append(freq_tmp)

    # compute the period  
    T = []
    for f in freq:
	t=1./f #compute the period
        T.append(t)
  
    # convert T, time and hv into numpy arrays
    T = np.array(T)
    freq = np.array(freq)
    time = np.array(time) 
    hv = np.array(hv)
    baz = np.array(azi)
    dop = np.array(dop)

    # compute the EQ time in seconds
    [hh,mm,ss] = EQtimes[kk].split(":")
    EQtimeSec = datetime.timedelta(hours=int(hh), minutes=int(mm), seconds=int(ss)).total_seconds()
#---------------------------------------------------------------------
# Plot the results 

    fig=plt.figure(2*kk, figsize=(11.69, 8.27) )
    plt.rcParams['xtick.labelsize']=14
    plt.rcParams['ytick.labelsize']=14
    
    plt.subplot2grid((6,2),(0,0), rowspan=3) 
    
    # load the station meta data
    locID = flB.split('.')[4]
    staName = glob.glob("%s/resp/STXML*%s.BHZ" %(baseDir, locID))[0]
    staDta = read_inventory(staName)
    lats=staDta[0][0].latitude
    lons=staDta[0][0].longitude
    
    m= Basemap(projection='aeqd' , width=28000000, height=28000000, 
            lat_0 = lats, lon_0 = lons)
    m.shadedrelief(scale=0.1)
    
    x, y =  m(lons, lats)
    m.scatter(x,y,200,color="r",marker="v",edgecolor="k",zorder=3)
    
    x, y = m(EQlons[kk], EQlats[kk]) 
    #m.scatter(x,y,200, color="b", marker=".", edgecolor="k", zorder=3)
    # plot the great circles
    m.drawgreatcircle(lons,lats,EQlons[kk],EQlats[kk],
            linewidth=2,color='k',alpha=0.5)
    ax =plt.gca()
    b = beach(MT[kk,:], xy=(x,y), width=2000000, linewidth=1.5)
    b.set_zorder(10)
    ax.add_collection(b)
    # plot the great circles
    #m.drawgreatcircle(lons,lats,EQlons[kk],EQlats[kk],
    #        linewidth=2,color='k',alpha=0.5)
   
    plt.title("%s \n Depth %s km - Ms %s" % (EQtimes[kk],EQdepth[kk], EQmag[kk]), size=18)

    # histogram of the back azimuth
    nBins = 360/10
    hBins = np.arange(-180,190,10) # create bins centred on the values
    axP = plt.subplot2grid((6,2), (0,1), rowspan=3, polar=True)
    # setting the axis to be in geographical coordinates
    axP.set_theta_direction(-1)
    axP.set_theta_offset(np.pi/2.0)
    # generate a polar histogram of the back azimuth as a function of frequency
#    deltaF = np.diff(np.sort(freq_list))[0]   
    deltaF = 0.01    
#    fBins = np.arange(0.005, 0.5, deltaF)
    fBins = np.linspace(np.min(freq_list), np.max(freq_list), 25)
    deltaA = np.deg2rad(5)
    aBins = np.arange(-np.pi, np.pi+deltaA, deltaA) # angular bins
    thetaBin, rBin = np.meshgrid(aBins, fBins)
#    bazR = np.mod(np.deg2rad(baz), 2*np.pi)
    bazR = np.deg2rad(baz)

    # compute the 2D histogram 
    [bazFreqH, xbin, ybin] = np.histogram2d(x=freq, y=bazR, 
            bins=[fBins, aBins])
    # normalize the plot based on the maximum  
    bazFreqMax = bazFreqH.max()
    bazFreqMin = bazFreqH.min()
    bazFreqN = (bazFreqH -bazFreqMin) / (bazFreqMax - bazFreqMin)  
    # plot the results
    normC = colors.Normalize(vmin=bazFreqMin, vmax=bazFreqMax)
   
    pc=axP.pcolor(thetaBin, rBin, bazFreqN, 
            edgecolors='w', linewidths=0.25,
            cmap='Spectral_r')
    # mask values below the minimum we have 
    axP.set_ylim([0, 0.5])
    #set the angular tick labels
    thetaticks = np.arange(0,360,45)
    axP.set_thetagrids(thetaticks, frac=1.15) 
    cbar = plt.colorbar(pc,pad=0.1)
    cbar.ax.set_ylabel('Normalised count', fontsize=16)
    # remove the radial tick labels
    axP.set_yticklabels([])

# OLD 2D histogram 
#    [n_all, bins_all, patch] = plt.hist(baz,hBins)
#    # filter the back azimuth data into the frequency band 0-0.1Hz and plot histogram
#    idx = np.where(freq <=0.1) 
#    # compute the backazimuth based on the source and station position 
#    n, Nbins, patches = plt.hist(baz[idx],hBins,alpha=0.8,facecolor='r')
#    [gc_dist, gc_baz, gc_az] = gps2dist_azimuth(lats, lons, EQlats[kk], EQlons[kk])
#    # these values are set between 0--360 degrees but our plots are -180--180 so we need
#    # to convert our angles
#    if gc_baz > 180:
#        gc_baz = -180 + (gc_baz % 180)
#    
#    # add a line to show the theoretical back azimuth of the event and the peak back azimuth
#    # peak back azimuth 
#    mids = bins_all[:-1] + np.diff(bins_all)/2. 
#    peakMax=mids[n_all.argmax()]
#    plt.axvline(x=peakMax, color='k', linestyle='solid', linewidth=2) 
#    # find the peak value of the back azimuth 
#    mids = Nbins[:-1] + np.diff(Nbins)/2.
#    plt.axvline(x=mids[n.argmax()], color='k', linestyle='dashed', linewidth=2)
#
#    plt.xlim(-180,180)
#    plt.xlabel('Back azimuth ($^\circ$)',size=16)
#    plt.ylabel('Counts',size=16)
#    plt.grid()
#    
#    plt.legend(['Peak all freq %2.1f$^\circ$' %peakMax, 
#        'Peak freq.<0.1Hz %2.1f$^\circ$' %mids[n.argmax()],
#        'All freq.', 'Freq. < 1.1 Hz'], loc='best', framealpha=0.7)
#    # find the peak value of the back azimuth 
#    mids = Nbins[:-1] + np.diff(Nbins)/2. 
#    plt.title("EQ back azimuth %2.1f $^\circ$" %gc_baz, size=18)
#        
#
#    plt.subplot2grid((6,2), (3,0), rowspan=3) 
#    plt.scatter(freq, time, 20,c = baz, marker=".",cmap="hsv",edgecolor="face", zorder=4)    
#    cb = plt.colorbar()
#    cb.ax.set_title("BAZ ($^\circ$)",size=14)
#
#    plt.xlim([0.005, 0.5])
#    plt.ylim([0, day_sec])
#    hourTick = np.arange(0,25,4).tolist()
#    hourTickMark = ["%s" % x for x in hourTick]
#    hourTick = np.multiply(hourTick,60.*60.).tolist() # convert to seconds 
#    hourTick.append(EQtimeSec) # append EQ time
#    hourTickMark.append("EQ")
#    plt.yticks(hourTick,hourTickMark)
#    label = plt.gca().yaxis.get_ticklabels()[-1]
#    label.set_color('red')
#    plt.ylabel("Time (Hours)",size=16)
#    plt.xlabel("Frequency (Hz)", size=16)
#    plt.grid() 
#------------------------------------------------------------------------------
#   Plot the sea pressure for the given day` 
    nc = NetCDFFile('sea-pressure-%s-%s-%s.nc' %(year, month, day))
    lats1 = nc.variables['lat'][:]
    lons1 = nc.variables['lon'][:]
    nlats = len(lats1)
    nlons = len(lons1)

    # read prmsl, convert to hPa (mb).
    slp = nc.variables['slp'][0]
    # the window parameter controls the number of highs and lows detected.
    # (higher value, fewer highs and lows)
    local_min, local_max = extrema(slp, mode='wrap', window=50)
    
    plt.subplot2grid((6,2),(3,0), rowspan=3)  
    # create the basemap 

#    m = Basemap(projection = 'ortho', lon_0 = lons, lat_0 = lats,
#            llcrnrx=-650000, llcrnry=-1900000, urcrnrx=1200000, urcrnry=2000000, 
#            resolution = 'h', anchor='W')
#
    m= Basemap(projection='stere' , width=5500000, height=7000000, 
            lat_0 = lats, lon_0 = lons)
    # plot the station location
    latsSta=staDta[0][0].latitude
    lonsSta=staDta[0][0].longitude
    xS, yS =  m(lonsSta, latsSta)
    m.scatter(xS,yS,200,color="r",marker="v",edgecolor="k",zorder=3)

    # add the sea-pressure information
    slp, lons1 = addcyclic(slp, lons1)
    # find x, y of map projection grid
    lons1, lats1 = np.meshgrid(lons1,lats1)
    x, y = m(lons1,lats1)
    cs = m.contourf(x,y,slp,
            np.arange(np.floor(np.min(slp)),np.floor(np.max(slp)),10),
            cmap='GnBu')
    m.shadedrelief()
    m.drawcoastlines()
     
    # histogram of the azimuth computations and find the angle where this is a max
    [n_all, bins_all] = np.histogram(bazR, aBins)
    n_norm = n_all/float(n_all.max()) # normalize the histogram 
    idx = np.where(n_norm >= 0.5)[0] #get the bins which are 50% of the max
    mids = bins_all[:-1] + np.diff(bins_all)/2.
    bins_n = mids[idx]
    
    latP, lonP = getLatLon(latsSta, lonsSta, np.rad2deg(bins_n), 2000)
    # compute new lat and lon from station along the azimuth
    xP, yP = m(lonP, latP)    
    
    # black and white scatter plot with the colors giving the normalised
    # histogram count
    cmap = plt.get_cmap('hot',10)
    bounds = np.linspace(0.5,1.0,11)
    norm = colors.BoundaryNorm(bounds, cmap.N) 
    
    css = m.scatter(xP, yP, 40,marker='s', c=n_norm[idx], cmap=cmap, norm=norm,
            edgecolor="face")
    


    # colorbar
    cb2 = plt.colorbar(css, ticks=np.linspace(0.5,1,6), pad=0.15)
    cb2.set_label("Normalised count",size=16)   
    cb = plt.colorbar(cs)
    cb.set_label("Sea Level Pressure (mb)",size=16)

    # Plot the traces used to analyise the data
    dtaFolder = '%s/processed/*.%s*BHZ*.sac' %(baseDir, locID)
    st = read(dtaFolder)        
    st += read(dtaFolder.replace("BHZ", "BHN"))
    st += read(dtaFolder.replace("BHZ", "BHE"))
    st = st.merge()
    st = st.sort()
    kkk=3
    for tr in st:
        comp = tr.stats.channel
        ax = plt.subplot2grid((6,2),(kkk,1))
        
        trTime = np.arange(0, tr.stats.npts, 1)*tr.stats.delta
        trTime_h = trTime/60.
        hourTick = np.arange(0,24+1,4).tolist()
        
        plt.plot(trTime, tr.data, '-k', linewidth=2)

        # set the y axis to have only 3 ticks 
        ax.yaxis.tick_right()
        ax.yaxis.set_ticks_position('both')
        yT = np.array([min(plt.yticks()[0]), 0,max(plt.yticks()[0])])
        yTMark = ["%1.1e" %x for x in yT] 
        plt.yticks(yT, yTMark)
        if kk == 4:
            plt.ylabel('Displacement (m)', size=16)
        
        if kkk == 5:
            # setting up the tick labels and markers
            hourTickMark = ["%s" % x for x in hourTick]
            hourTick = np.multiply(hourTick,60.*60.).tolist() # convert to seconds 
            plt.xticks(hourTick,hourTickMark)
            plt.xlabel('Time (Hours)', size=16)
        else: 
            hourTickMark = ["" for x in hourTick]
            hourTick = np.multiply(hourTick,60.*60.).tolist() # convert to seconds 
            plt.xticks(hourTick,hourTickMark)
        
        plt.xlim([0, 24*60.*60.])
        plt.legend(['%s' %comp], loc='upper right', framealpha=0.7)
        plt.grid()
        kkk+=1

#    plt.subplot(2,2,4) 
#    plt.scatter(freq, time, 20,c = dop, marker=".",cmap="jet",edgecolor="face", zorder=4)    
#    cb = plt.colorbar()
#    cb.ax.set_title("DOP (%)",size=14)
#    plt.xlim([0.015, 0.2])
#    plt.ylim([0, day_sec])
#    plt.yticks(hourTick,hourTickMark)
#    plt.xlabel("Frequency (Hz)", size=16)
#    label = plt.gca().yaxis.get_ticklabels()[-1]
#    label.set_color('red')
#    plt.ylabel("Time (Hours)",size=16)
#    plt.grid() 
#    # subplot adjust
#    #if kk==0:
#    #    plt.subplots_adjust(left=0.05, wspace=0.3)
#    #else:
#    #    plt.subplots_adjust(left=0, wspace=0.06)
#    plt.subplots_adjust(left=0.08, wspace=0.12)
    fig.suptitle("%s %02d-%s-%s" %(station,day, month, year), size=18)
    #---------------------------------------------------- 
    # plot of the back azimuth and degree of polarization
    fig=plt.figure(2*kk+1, figsize=(11.69, 10.27) )
    plt.rcParams['xtick.labelsize']=16
    plt.rcParams['ytick.labelsize']=16
    
    # scatter plot of the H/V as a function of period and time
    plt.subplot(1,2,1)
    plt.scatter(freq, time, 20,c = baz, marker=".",cmap="hsv",edgecolor="face", zorder=4)    
    cb = plt.colorbar()
    cb.ax.set_title("BAZ ($^\circ$)",size=16)

    plt.xlim([0.015, 0.2])
    plt.ylim([0, day_sec])
    hourTick = np.arange(0,25,4).tolist()
    hourTickMark = ["%s" % x for x in hourTick]
    hourTick = np.multiply(hourTick,60.*60.).tolist() # convert to seconds 
    hourTick.append(EQtimeSec) # append EQ time
    hourTickMark.append("EQ")
    plt.yticks(hourTick,hourTickMark)
    plt.ylabel("Time (Hours)",size=18)
    plt.xlabel("Frequency (Hz)", size=18)
    plt.grid() 

    plt.subplot(1,2,2)
    plt.scatter(freq, time, 20,c = dop, marker=".",cmap="jet",edgecolor="face", zorder=4)    
    cb = plt.colorbar()
    cb.ax.set_title("DOP (%)",size=18)
    plt.xlim([0.015, 0.2])
    plt.ylim([0, day_sec])
    plt.yticks(hourTick,hourTickMark)
    plt.xlabel("Frequency (Hz)", size=18)
    plt.ylabel("Time (Hours)",size=18)
    plt.grid() 
    plt.subplots_adjust(top=0.88, left=0.1, wspace=0.35)
    fig.suptitle("%s %02d-%s-%s\n %s - Ms %s" %(station,day, month, year,
        EQtimes[kk], EQmag[kk]), size=20)
    
    
plt.show()
