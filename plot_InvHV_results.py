#!/usr/bin/env python
#
# Script to plot the reulst of the inversion
#
import matplotlib.pylab as plt
import numpy as np 
import sys
import os
import operator
import glob
import argparse
import matplotlib.gridspec as gridspec
from calendar import  month_name
from matplotlib.pylab import cm
from math import isnan
#---------------------------------------------------------------------
def normalize_misfit(m, m1, m2, g1, g2):
    # 
    #                   (originalValue - minOrigRange)(maxNewRange - minNewRange)
    # y = minNewRange + ---------------------------------------------------------
    #                               (maxOrigRange - minOrgiRange)
	d = m1 - g1
	m1s = 0
	m2s = m2 - m1
	ms = m - m1
	g1s = 0
	g2s = g2 - g1 
	x = g1 + ms * g2s / m2s
	
	return x
#---------------------------------------------------------------------
def getResults(baseDir, fname):
    var = "%s/%s" %(baseDir, fname)
    # check to make sure file exists in current directory
    if os.path.isfile(var):
        print '--> Plotting results from %s' %var
    else:
        print "ERROR: %s does not exist" % var
        sys.exit()
    return var

#---------------------------------------------------------------------
def getObs(baseDir, fname):
    var = "%s/%s" %(baseDir, fname)
    # check to make sure file exists in current directory
    if os.path.isfile(var):
        print '--> Opening %s ' %var
    else:
        print "ERROR: %s does not exist" % var
        sys.exit()
    return var

#---------------------------------------------------------------------
def getInputModel(baseDir, fname):
    var = "%s/%s" %(baseDir, fname)
    # check to make sure file exists in current directory
    if os.path.isfile(var):
        print '--> Opening input model %s ' %var
    else:
        print "ERROR: %s does not exist" % var
        sys.exit()
    return var

#
#---------------------------------------------------------------------
def get_cost(cost_sorted, threshold, vp, vs, z, min_cost):
    costT = []
    costNT=[]
    VpT = []
    VsT = []
    ZT = []
    for ii in range(0, len(cost_sorted)):
        index = cost_sorted[ii][1]
        cost = cost_sorted[ii][0]
        if cost <= threshold:
            colorVal = normalize_misfit(cost, min_cost, threshold,
                    0.0, 1.0)

            costT.append(cost)
            costNT.append(colorVal)
            VpT.append(vp[index])
            VsT.append(vs[index])
            ZT.append(z[index])

    return np.array(costT), np.array(costNT), np.array(VpT), \
            np.array(VsT), np.array(ZT)
#---------------------------------------------------------------------
def compMsft(dta,model,sd):
    res = ((np.array(dta) - np.array(model))**2) /(np.array(sd)**2)
    misfit = np.sum(res)
    return misfit

#---------------------------------------------------------------------
def main():
    '''
    Script to take the inversion results and plot the Vp, Vs, rho profiles
    as well as the trade off of each parameter.

    The directory sturcture is assumed to be /station/Inversion/Results/YYYY/MM
    Where all files are expected to reside except for the litho1.0 model which is
    assumed to be in /station/Inversion
    '''
    parser = argparse.ArgumentParser(description=
                "Plot the results of the inversion as Vp, Vs, rho depth profiles,\n"
                "data comparison and parameter trade off curves\n"
                "\n" 
                "The directory sturcture is assumed to follow:\n"
                "/station/Inversion/Results/YYYY/MM \n"
                "Where all files are expected to reside except for the litho1.0 \n"
                "model which is assumed to be in /station/Inversion",
                formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--station', type=str,
                    help='station name to process', required=True)
    parser.add_argument('--year', nargs=1,
                    help='year to process', required=True )
    parser.add_argument('--month', nargs=1,
                    help='month to process', required=True)
    parser.add_argument('--results',type=str,
                    help='inversion results output file', required=True)
    parser.add_argument('--obs', type=str,
                    help='data observation file used for inversion',
                    required=True)
    parser.add_argument('--nLayers', type=int,
                    help='number of layers to compute the trade off plots',
                    required=True)
    parser.add_argument('--litho', nargs=1,
                    help='litho1.0 modelled data. It is assumed that the file is \n'
                    'in the Inversion directory. Default name is station.litho1.0.d',
                    required=False)
    parser.add_argument('--lithoDta', nargs=1,
                    help='results of the forward modelling using the litho1.0 model\n'
                    'It is assumed the file is in the Inversion directory \n'
                    'Default name for the file is station.predicted_data_order1.txt',
                    required=False)
    parser.add_argument('--thresh', type=float,
                    help='percentage threshold used to highlight best models. \n'
                    'Default is 20', 
                    required=False)

    # setting up the variables
    args = vars(parser.parse_args())
    station = args['station']
    year = args['year'][0]
    month = args['month'][0]
    mn = month_name[int(month)]
    nLayers = args['nLayers']

    if args['thresh'] is None:
        perc_threshold = 20.
    else:
        perc_threshold = args(['thresh'])
    
    if args['litho'] is None:
        lMdl = "%s.litho1.0.d" % station
    else:
        lMdl = args(['lithoDta'])

    if args['lithoDta'] is None:
        lDta = "%s.predicted_data_order1.txt" % station
    else:
        lDta = args(['lithoDta'])

    baseDir = "%s/Inversion" % station
    dirname = "%s/Results/%s/%02d" %(baseDir,year, int(month))
    # try and load the various input files
    result_filename = getResults(dirname, args['results'])
    obs_data_filename = getObs(dirname, args['obs'])
    input_model_filename = getInputModel(baseDir, lMdl)

    # read observed data
    TT = []
    E_obs = []
    sd_obs = []
    lines = open(obs_data_filename).readlines()
    for i in range(1,len(lines)):
	T = float(lines[i].split()[0])
	E = float(lines[i].split()[1])
	sd = float(lines[i].split()[2])
	TT.append(T)
	E_obs.append(E)
	sd_obs.append(sd)

    # get the length of the data to be plotted
    n_data = len(TT) 

    # read input model
    lines = open(input_model_filename).readlines()
    zz_input = [0.0]

    vvp_input = [float(lines[12].split()[1])]
    vvs_input = [float(lines[12].split()[2])]
    rho_input = [float(lines[12].split()[3])]

    depth = 0.0
    for i in range(12,len(lines)):
        H = float(lines[i].split()[0])
        vp = float(lines[i].split()[1])
        vs = float(lines[i].split()[2])
        rho = float(lines[i].split()[3])

        depth += H
        zz_input.append(depth)
        zz_input.append(depth)
        vvp_input.append(vp)
        vvs_input.append(vs)
        rho_input.append(rho)
        if i == len(lines)-1:
            vvp_input.append(vp)
            vvs_input.append(vs)
            rho_input.append(rho)
        else:
            vvp_input.append(float(lines[i+1].split()[1]))
            vvs_input.append(float(lines[i+1].split()[2]))
            rho_input.append(float(lines[i+1].split()[3]))

    # read the results from the forward modelling of litho1.0
    try:
        mfile = station + "/Inversion/" + lDta 
        tmp = np.loadtxt(mfile)
    except EXception as error:
        print('WARNING: %s' %error) 
        sys.exit()
    Tm = tmp[:,0]
    Em = tmp[:,1]
    #=====================================
    # read result file
    lines = open(result_filename).readlines()
    model_index = []
    for i in range(0,len(lines)):
        if lines[i].split()[0] == "mft":
	    model_index.append(i)
    predicted_data = []
    vvs_list = []
    vvp_list = []
    rho_list = []
    zz_list = []
    cost_list = []
    n = 0
    for i in model_index:
        mft = float(lines[i].split()[1])
        cost = float(lines[i].split()[3])
        if not isnan(cost):
	    cost_list.append([cost,n])
	    E_curve = []
		
	    for j in range(2, n_data+2):
	        E_curve.append(float(lines[i+j]))

	    predicted_data.append(E_curve)
	    depth = 0.0
	    zz = [0.0]
	    vvs = [float(lines[i+n_data+15].split()[2])]
            vvp = [float(lines[i+n_data+15].split()[1])]
            rrho = [float(lines[i+n_data+15].split()[3])]

	    for j in range(n_data+15, 100000):
			
	        if lines[i+j+1].split()[0][0] != "#": 
		    H = float(lines[i+j].split()[0])
		    vs = float(lines[i+j].split()[2])
		    vp = float(lines[i+j].split()[1])
		    rho = float(lines[i+j].split()[3])
		    depth += H

		    zz.append(depth)
		    zz.append(depth)
		    vvs.append(vs)
		    vvs.append(float(lines[i+j+1].split()[2]))
                    vvp.append(vp)
		    vvp.append(float(lines[i+j+1].split()[1]))
                    rrho.append(rho)
		    rrho.append(float(lines[i+j+1].split()[3]))
	        else:
		    break
	
	    vvs_list.append(vvs)
	    zz_list.append(zz)
            vvp_list.append(vvp)
            rho_list.append(rrho)
	    n += 1

    # sort the models by their cost function
    cost_sorted =  sorted(cost_list, key=operator.itemgetter(0), reverse=True)
    min_cost = cost_sorted[-1][0]
    max_cost = cost_sorted[0][0]
    
    threshold = min_cost + ((perc_threshold/100.) * min_cost)

    #=====================================
    thresh_idx=[]
    # Plot the results  
    plt.figure(1, figsize=(10,11))
    plt.rcParams['xtick.labelsize']=16
    plt.rcParams['ytick.labelsize']=16

    n_plotted = 0
    for i in range(0,len(cost_sorted)):
        index = cost_sorted[i][1]
        cost = cost_sorted[i][0]
        # vp
        plt.subplot(234)
        plt.plot(vvp_list[index], zz_list[index], color="0.95", zorder=0, linewidth=3)
        # vs
        plt.subplot(235) 
        plt.plot(vvs_list[index], zz_list[index], color="0.95", zorder=0, linewidth=3) 
        # rho
        plt.subplot(236) 
        plt.plot(rho_list[index], zz_list[index], color="0.95", zorder=0, linewidth=3) 


#	plt.subplot(211)
#	plt.plot(TT, predicted_data[index], zorder=0, color="0.95", linewidth=3)

        if cost <= threshold:
		#colorVal = 1-normalize_misfit(cost, min_cost, threshold, 0.2,1.)
	    colorVal = normalize_misfit(cost, min_cost, threshold, 0.2,1.)
	    plt.subplot(234)
	    plt.plot(vvp_list[index], zz_list[index], c=str(colorVal), linewidth=2)
            plt.subplot(235)
	    plt.plot(vvs_list[index], zz_list[index], c=str(colorVal), linewidth=2)
            plt.subplot(236)
	    plt.plot(rho_list[index], zz_list[index], c=str(colorVal), linewidth=2)

	    n_plotted += 1

	    plt.subplot(211)
	    plt.plot(TT, predicted_data[index], c=str(colorVal), zorder=2)

            thresh_idx.append(index)
        best_index = index

    plt.subplot(234)
    plt.plot(vvp_list[best_index], zz_list[best_index], linestyle="-", 
            color="red", linewidth=3, label="Best model",zorder=2) 
    plt.plot(vvp_input, zz_input, linestyle="-", color="blue", linewidth=3, 
            label="Input model", zorder=1)
    plt.ylim(10,0) 
    xTicks = np.arange(2,10,2) 
    xTickMark = ["%s" % x for x in xTicks]
    plt.xticks(xTicks, xTickMark)
    plt.xlabel("Vp (km/s)", fontsize=20)
    plt.ylabel("Depth (km)", fontsize=20)
    plt.legend(loc='lower center', fontsize=16)
    plt.grid()
    
    plt.subplot(235)
    plt.plot(vvs_list[best_index], zz_list[best_index], linestyle="-", 
            color="red", linewidth=3, label="Best model",zorder=2)
    plt.plot(vvs_input, zz_input, linestyle="-", color="blue", linewidth=3, 
            label="Input model", zorder=1)
    plt.ylim(10,0)
    plt.xlabel("Vs (km/s)", fontsize=20)
    xTicks = np.arange(2,6,1)
    xTickMark = ["%s" % x for x in xTicks]
    plt.xticks(xTicks, xTickMark)
    #plt.ylabel("Depth (km)", fontsize=20)
    plt.legend(loc='lower center', fontsize=16)
    plt.grid()
    
    plt.subplot(236)
    plt.plot(rho_list[best_index], zz_list[best_index], linestyle="-", 
            color="red", linewidth=3, label="Best model",zorder=2)
    plt.plot(rho_input, zz_input, linestyle="-", color="blue", linewidth=3, 
            label="Input model", zorder=1)
    plt.ylim(10,0)
    xTicks = np.arange(1,4,1)
    xTickMark = ["%s" % x for x in xTicks]
    plt.xticks(xTicks, xTickMark)
    plt.xlabel(r'$\rho$ (kg/m$^3$)', fontsize=20)
    #plt.ylabel("Depth (km)", fontsize=20)
    plt.legend(loc='lower center', fontsize=16)
    plt.grid()
    
    plt.subplot(211)
    plt.semilogx(TT, predicted_data[best_index], color="red", 
            marker='o', linestyle="-", linewidth=2, label="H/V from best model")
    plt.errorbar(TT, E_obs, yerr=sd_obs, fmt=" ", color="grey", linewidth=1.5)
    plt.scatter(TT, E_obs, color="black",label="Observed data", zorder=5)
    
    plt.semilogx(Tm, Em, color="blue", linestyle="-", 
            linewidth=2, label="litho1.0")
    plt.xlabel("Period (s)", fontsize=20)
    plt.ylabel("log(H/V)", fontsize=20)
    plt.legend(fontsize=16, scatterpoints=1,loc='lower left')
    
    xTicks = np.arange(3,11,2)
    xTickMark = ["%s" % x for x in xTicks]
    plt.xticks(xTicks, xTickMark)
    plt.ylim(-0.5,0.5)
    plt.xlim(2.9,10)
    plt.grid()
    
    misfit = compMsft(E_obs,predicted_data[best_index],sd_obs)
    mt = "Data misfit = %3.3f" % misfit
    plt.text(6,-0.4, mt, fontsize=18)
    pltTitle = "%s %s %s" %(station, mn, year)
    plt.suptitle(pltTitle, fontsize=20)
    
    plt.show()
    print '--> Best Model: vs=%s' %vvs_list[best_index] 
    print '--> Best Model: z=%s' %zz_list[best_index] 
    sys.exit()
    #=====================================
    # Plot the parameter trade offs

    vsVals=[]
    vpVals=[]
    zVals=[]
    for idx in range(0,len(vvs_list)):
        tmpP = vvp_list[idx][0::2]
        tmpS = vvs_list[idx][0::2]
        tmpZ = zz_list[idx][0::2]
        vpVals.append(tmpP[:nLayers])
        vsVals.append(tmpS[:nLayers])
        zVals.append(tmpZ[1:nLayers+1])

    # get the points which are below 
    costT, costNT, VpT, VsT, ZT = get_cost(cost_sorted, threshold, 
        vpVals, vsVals, zVals, min_cost)

    # convert the data into numpy vectors
    vpVals=np.array(vpVals)
    vsVals=np.array(vsVals)
    zVals=np.array(zVals)
    
    fig2 = plt.figure(2, figsize=(12,8))
    fig3 = plt.figure(3, figsize=(12,8))
    fig4 = plt.figure(4, figsize=(12,8))
    plt.rcParams['xtick.labelsize']=16
    plt.rcParams['ytick.labelsize']=16
    spec = gridspec.GridSpec(ncols=nLayers, nrows=nLayers)
    for nLx in range(0,nLayers):
        for nLy in range(nLx, nLayers):
            f_ax = fig2.add_subplot(spec[nLy,nLx])
            f_ax3 = fig3.add_subplot(spec[nLy,nLx])
            f_ax4 = fig4.add_subplot(spec[nLy,nLx])
            # plot the results as grey dots 
            f_ax.plot(vsVals[:,nLx], vsVals[:,nLy], '.', c='lightgrey', 
                    markersize=2, zorder=1)
            # colour the values which are within the top 80%
            fs1=f_ax.scatter(VsT[:,nLx], VsT[:,nLy], s=25, vmin=0, vmax=1, 
                    c=costNT, cmap='viridis', edgecolor='face', zorder=2)
            # check to see if we can add a colorbar
            if nLx==(nLayers - 1) and nLy==(nLayers - 1):
                # get the axis of the last figure
                box = f_ax.get_position()
                # create color bar
                axColor = fig2.add_axes([box.x0 + box.width * 1.03, 
                    box.y0, 0.01, box.height])
                cb = plt.colorbar(fs1, cax = axColor, orientation="vertical")
                cb.set_label('% from minima', fontsize=18)
                cb.set_ticks([0,0.25, 0.5, 0.75,1])
                cbTicks = [0,perc_threshold/4,perc_threshold/2, 
                        3*perc_threshold/4 ,perc_threshold]
                cbTickMark = ["%s" % x for x in cbTicks]
                cb.set_ticklabels(cbTickMark) 
            #
            f_ax.plot(vsVals[best_index,nLx], vsVals[best_index,nLy], '+', c='red', 
                    markersize=10, zorder=3, markeredgewidth=2)
            #===============
            f_ax3.plot(vpVals[:,nLx], vpVals[:,nLy], '.', c='lightgrey', 
                    markersize=2, zorder=1)
            # colour the values which are within the top 80%
            fs3=f_ax3.scatter(VpT[:,nLx], VpT[:,nLy], s=25, vmin=0, vmax=1, 
                    c=costNT, cmap='viridis', edgecolor='face', zorder=2)

            # check to see if we can add a colorbar
            if nLx==(nLayers - 1) and nLy==(nLayers - 1):
                # get the axis of the last figure
                box = f_ax3.get_position()
                # create color bar
                axColor = fig3.add_axes([box.x0 + box.width * 1.03, 
                    box.y0, 0.01, box.height])
                cb = plt.colorbar(fs3, cax = axColor, orientation="vertical")
                cb.set_label('% from minima', fontsize=18)
                cb.set_ticks([0,0.25, 0.5, 0.75,1])
                cbTicks = [0,perc_threshold/4,perc_threshold/2, 
                        3*perc_threshold/4 ,perc_threshold]
                cbTickMark = ["%s" % x for x in cbTicks]
                cb.set_ticklabels(cbTickMark) 
            #             
            f_ax3.plot(vpVals[best_index,nLx], vpVals[best_index,nLy], '+', c='red', 
                    markersize=10, zorder=3, markeredgewidth=2)

            #===============
            if nLx==nLy:
                if nLx==0:
                    f_ax4.plot(zVals[:,nLx], zVals[:,nLy], '.', c='lightgrey', 
                        markersize=2, zorder=1)
                    # colour the values which are within the top 80%
                    f_ax4.scatter(ZT[:,nLx], ZT[:,nLy], s=25, vmin=0, vmax=1, 
                        c=costNT, cmap='viridis', edgecolor='face', zorder=2)
                    #
                    f_ax4.plot(zVals[best_index,nLx], 
                        zVals[best_index,nLy], 
                        '+', c='red', markersize=10, zorder=3, markeredgewidth=2)
                    # get the min/max of what's plotted to be used later
                    f_ax4minX = np.floor(zVals[:,nLx].min())
                    f_ax4maxX = np.ceil(zVals[:,nLx].max())
                    f_ax4minY = np.floor(zVals[:,nLy].min())
                    f_ax4maxY = np.ceil(zVals[:,nLy].max())
                else:
                    f_ax4.plot(zVals[:,nLx] - zVals[:,nLx-1], 
                        zVals[:,nLy] - zVals[:,nLy-1], 
                        '.', c='lightgrey', 
                        markersize=2, zorder=1)
                    # colour the values which are within the top 80%
                    fs4 = f_ax4.scatter(ZT[:,nLx] - ZT[:,nLx-1], 
                        ZT[:,nLy] - ZT[:,nLy - 1], s=25, vmin=0, vmax=1, 
                        c=costNT, cmap='viridis', edgecolor='face', zorder=2)
                    # check to see if we can add a colorbar
                    if nLx==(nLayers - 1) and nLy==(nLayers - 1):
                        # get the axis of the last figure
                        box = f_ax4.get_position()
                        # create color bar
                        axColor = fig4.add_axes([box.x0 + box.width * 1.03, 
                            box.y0, 0.01, box.height])
                        cb = plt.colorbar(fs4, cax = axColor, orientation="vertical")
                        cb.set_label('% from minima', fontsize=18)
                        cb.set_ticks([0,0.25, 0.5, 0.75,1])
                        cbTicks = [0,perc_threshold/4,perc_threshold/2, 
                            3*perc_threshold/4 ,perc_threshold]
                        cbTickMark = ["%s" % x for x in cbTicks]
                        cb.set_ticklabels(cbTickMark) 

                    #
                    f_ax4.plot(zVals[best_index,nLx] - zVals[best_index,nLx-1], 
                        zVals[best_index,nLy] - zVals[best_index,nLy-1],
                        '+', c='red', markersize=10, zorder=3, markeredgewidth=2)
                    # get the min/max of what's plotted to be used later
                    f_ax4minX = np.floor((zVals[:,nLx] - zVals[:,nLx-1]).min())
                    f_ax4maxX = np.ceil((zVals[:,nLx] - zVals[:,nLx-1]).max())
                    f_ax4minY = np.floor((zVals[:,nLy] - zVals[:,nLy-1]).min())
                    f_ax4maxY = np.ceil((zVals[:,nLy] - zVals[:,nLy-1]).max())
            else: 
                f_ax4.plot(zVals[:,nLx], zVals[:,nLy]- zVals[:,nLx], '.', 
                    c='lightgrey', markersize=2, zorder=1)
                # colour the values which are within the top 80%
                f_ax4.scatter(ZT[:,nLx], ZT[:,nLy] - ZT[:,nLx], s=25, 
                    vmin=0, vmax=1, c=costNT, cmap='viridis', edgecolor='face', 
                    zorder=2)
            #
                f_ax4.plot(zVals[best_index,nLx], 
                    zVals[best_index,nLy] - zVals[best_index,nLx], 
                    '+', c='red', markersize=10, zorder=3, markeredgewidth=2)
                # get the min/max of what's plotted to be used later
                f_ax4minX = np.floor(zVals[:,nLx].min())
                f_ax4maxX = np.ceil(zVals[:,nLx].max())
                f_ax4minY = np.floor((zVals[:,nLy] - zVals[:,nLx]).min())
                f_ax4maxY = np.ceil((zVals[:,nLy] - zVals[:,nLx]).max())


            # Vs markers and ticks
            maxX = np.ceil(vsVals[:,nLx].max())
            minX = np.floor(vsVals[:,nLx].min())
            maxY = np.ceil(vsVals[:,nLy].max())
            minY = np.floor(vsVals[:,nLy].min())
            xTicks = np.arange(minX,maxX+0.1,1)
            yTicks = np.arange(minY,maxY+0.1,1)

#            xTicks = np.arange(1,5,1)
#            yTicks = np.arange(1,5,1)
            xTickMark = ["%s" % x for x in xTicks]
            yTickMark = ["%s" % y for y in yTicks]
            f_ax.set_xticks(xTicks)
            f_ax.set_xticklabels(xTickMark)
            f_ax.set_yticks(yTicks)        
            f_ax.set_yticklabels(yTickMark)
            f_ax.set_xlim(minX,maxX)
            f_ax.set_ylim(minY,maxY)
            f_ax.grid()
            # set the axes names
            if nLx==0:
                ylab = "Vs$_%i$ (km/s)" %(nLy+1)
                f_ax.set_ylabel(ylab, fontsize=20)
            if nLy==nLayers-1:
                xlab = "Vs$_%i$ (km/s)" %(nLx+1)
                f_ax.set_xlabel(xlab, fontsize=20)

            # Vp markers and ticks
            maxX = np.ceil(vpVals[:,nLx].max())
            minX = np.floor(vpVals[:,nLx].min())
            maxY = np.ceil(vpVals[:,nLy].max())
            minY = np.floor(vpVals[:,nLy].min())
            xTicks = np.arange(minX,maxX+0.1,1)
            yTicks = np.arange(minY,maxY+0.1,1)
#            xTicks = np.arange(3,8,2)
#            yTicks = np.arange(3,8,2)
            xTickMark = ["%s" % x for x in xTicks]
            yTickMark = ["%s" % y for y in yTicks]
            f_ax3.set_xticks(xTicks)
            f_ax3.set_xticklabels(xTickMark)
            f_ax3.set_yticks(yTicks)        
            f_ax3.set_yticklabels(yTickMark)
            f_ax3.set_xlim(minX,maxX)
            f_ax3.set_ylim(minY,maxY)
            f_ax3.grid()
            # set the axes names
            if nLx==0:
                ylab = "Vp$_%i$ (km/s)" %(nLy+1)
                f_ax3.set_ylabel(ylab, fontsize=20)
            if nLy==nLayers-1:
                xlab = "Vp$_%i$ (km/s)" %(nLx+1)
                f_ax3.set_xlabel(xlab, fontsize=20)

            # Z markers and ticks
#            maxX = np.ceil(zVals[:,nLx].max())
#            minX = np.floor(zVals[:,nLx].min())
#            maxY = np.ceil((zVals[:,nLy] - zVals[:,nLx]).max())
#            minY = np.ceil((zVals[:,nLy] - zVals[:,nLx]).min())
            if f_ax4maxX >0:
                xTicks = np.arange(f_ax4minX,f_ax4maxX + 0.1,0.5)
            else:
                xTicks = np.arange(0-f_ax4maxX/2, f_ax4maxX/2+0.1, 0.5)

            # check to make sure that maxY is greater than 0
            if f_ax4maxY > 0:
                yTicks = np.arange(f_ax4minY,f_ax4maxY + 0.1,0.5)
            else:
                yTicks = np.arange(0-f_ax4maxX/2,f_ax4maxX/2+0.1,0.5)

            xTickMark = ["%s" % x for x in xTicks]
            yTickMark = ["%s" % y for y in yTicks]
            f_ax4.set_xticks(xTicks)
            f_ax4.set_xticklabels(xTickMark)
            f_ax4.set_yticks(yTicks)        
            f_ax4.set_yticklabels(yTickMark)
            f_ax4.set_xlim(xTicks.min(),xTicks.max())
            f_ax4.set_ylim(yTicks.min(),yTicks.max())
            f_ax4.grid()
            # set the axes names
            if nLx==0:
                ylab = "Depth $_%i$ (km)" %(nLy+1)
                f_ax4.set_ylabel(ylab, fontsize=20)
            if nLy==nLayers-1:
                xlab = "Depth $_%i$ (km)" %(nLx+1)
                f_ax4.set_xlabel(xlab, fontsize=20)
    
    fig2.suptitle(pltTitle, fontsize=20)
    fig2.subplots_adjust(hspace=0.25)
    #
    fig3.suptitle(pltTitle, fontsize=20)
    fig3.subplots_adjust(hspace=0.25)
    #
    fig4.suptitle(pltTitle, fontsize=20)
    fig4.subplots_adjust(hspace=0.25)
    
    # Comparison of best vp, vs and rho
    plt.figure(5, figsize=(12,8))
    plt.rcParams['xtick.labelsize']=16
    plt.rcParams['ytick.labelsize']=16
    
    plt.subplot(131)
    plt.plot(vvp_list[best_index], zz_list[best_index], linestyle="-", 
            color="red", linewidth=3)
    plt.ylim(55,0)
    
    xTicks = np.arange(4,10,2)
    xTicks[0]=4
    xTickMark = ["%s" % x for x in xTicks]
    plt.xticks(xTicks, xTickMark)
    plt.xlabel("Vp (km/s)", fontsize=20)
    plt.ylabel("Depth (km)", fontsize=20)
    #plt.legend(loc='lower left', fontsize=16)
    plt.grid()
    
    plt.subplot(132)
    plt.plot(vvs_list[best_index], zz_list[best_index], linestyle="-", 
            color="red", linewidth=3)
    plt.ylim(55,0)
    plt.xlabel("Vs (km/s)", fontsize=20)
    xTicks = np.arange(2,6,1)
    xTickMark = ["%s" % x for x in xTicks]
    plt.xticks(xTicks, xTickMark)
    plt.grid()
    
    plt.subplot(133)
    plt.plot(rho_list[best_index], zz_list[best_index], linestyle="-", 
            color="red", linewidth=3)
    plt.ylim(55,0)
    plt.xlabel(r'$\rho$ (kg/m$^3$)', fontsize=20)
    xTicks = np.arange(1,4,1)
    xTickMark = ["%s" % x for x in xTicks]
    plt.xticks(xTicks, xTickMark)
    plt.grid()
    plt.suptitle(pltTitle, fontsize=20)
    
    plt.show()
    
    
    plt.figure(6, figsize=(8,6))
    plt.rcParams['xtick.labelsize']=16
    plt.rcParams['ytick.labelsize']=16
    
    for i in range(0,len(cost_list)):
        plt.scatter(i, cost_list[i][0], color="black", s=1)
    
    plt.yscale("log")
    plt.ylim(min_cost*0.9, max_cost)
    # get a sensible xlimit
    f = np.floor(np.log10(i))
    mxf = np.ceil(i/(10**f))*10**f 
    plt.xlim(0, mxf)
    plt.xlabel("# model", fontsize=20)
    plt.ylabel("Cost function", fontsize=20)
    plt.title(pltTitle, fontsize=20)
    plt.grid()
    plt.show()

#---------------------------------------------------------------------
if __name__=="__main__":
    main()
