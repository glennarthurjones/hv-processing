#!/usr/bin/env python
# 
# Python script to take the calculated H/V measurements and compare with
# the forward model using litho1.0

import glob
import numpy as np
import matplotlib.pyplot as plt
import argparse
from calendar import monthrange, month_name
from matplotlib import colors
#---------------------------------------------------------------------
def flipBaz(azi):
    '''
    Flip the back azimuth values to be between 0-360 
    '''
    baz = azi
    for jj, theta in enumerate(baz):
        if theta < 0:
            baz[jj] = 180 + theta % 180
            
    return baz 
#---------------------------------------------------------------------
def main():
    '''
    Script to take H/V results from compHV.py and compare with the forward
    problem computed using the script lithoForwardProlem.py
    '''

    parser = argparse.ArgumentParser(description=
                "Take the H/V measurements from compHV.py and compare with the\n"
                "synthetic H/V curves computed using lithoForwardProblem\n"
                "It is assumed that a synthetic dataset has been computed for the\n"
                "station using litho1.0 located in the directory ./station/Inversion/\n"
                "and named %station.predicted_data_order%order.txt",
                formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--station', type=str,
                    help='station name to process', required=True)
    parser.add_argument('--year', nargs=1,
                    help='year to process', required=True )
    parser.add_argument('--months', nargs='+',
                    help='months to process', required=True)
    parser.add_argument('--code', type=str,
                    help='measurement base code', 
                    required=True)
    parser.add_argument('--locID', nargs='+' ,
                    help='location ID of the station', 
                    required=True)
    parser.add_argument('--angles', nargs=2, type=float,
                    help='Optional angular filter to compute H/V stats.\n'
                    'The input should ordered minAng max Ang.Default=0-360', 
                    required=False)
    parser.add_argument('--minObs', type=int,
                    help='Minimum number of observations in frequency range to compute \n'
                    'H/Vstats. Default is to use all available observations', 
                    required=False)
    parser.add_argument('--order', type=int,
                    help='Order of models. Default is 0', required=False)
    parser.add_argument('--median', default=True, action='store_true',
                    help='Use to plot median values. Default option', required=False)
    parser.add_argument('--mean', dest='median', action='store_false',
                    help='Use to plot mean values.', required=False)

    # setting up the variables
    args = vars(parser.parse_args())
    station = args['station']
    year = args['year'][0]
    months = args['months'] 
    if len(months) > 1:
        months.sort(key=int)

    measurement_code = args['code']
    locID = args['locID']

    if args['angles'] is None:
        angTstr = "0-360"
    else:
        ang_thresh = args['angles']
        angTstr = '%d-%d' %(ang_thresh[0], ang_thresh[1])
    if args['order'] is None:
        order =  0
    else:
        order = int(args['order'])
    if args['minObs'] is None:
        minObs = 0 
    else:
        minObs = args['minObs']

    # loop over the months
    for kk, month in enumerate(months):
        dirname = "%s/%s.%02d" %(station, year, int(month))
        for loc in locID: 
            # load the relevant summary file
            if args['median']==True:
                fnameIn = ('%s/%s_%s_%s_%s.%02d_%d_%s_median.ell' 
                        %(dirname, station,loc, measurement_code, 
                        year, int(month),minObs, angTstr))
            else:
                fnameIn = ('%s/%s_%s_%s_%s.%02d_%d_%s_mean.ell' 
                        %(dirname, station,loc, measurement_code, 
                        year, int(month),minObs, angTstr))

            try:
                tmp = np.loadtxt(fnameIn, skiprows=1) 
            except Exception as error:
                print('WARNING: %s' %error)
                continue
            tt = tmp[:,0]
            hv = tmp[:,1]
            hv_err = tmp[:,2]
            # set-up the figure to be plotted
            fig = plt.figure(2*kk, figsize=(12,9))
            plt.rcParams['xtick.labelsize']=16
            plt.rcParams['ytick.labelsize']=16

            # load the synthetc dataset
            fnameSyn = ("%s/Inversion/%s.predicted_data_order%d.txt" 
                        %(station, station, order))
            try:
                tmp = np.loadtxt(fnameSyn)
            except Exception as error:
                print('WARNING: %s' %error)
                plt.clear(kk)
                continue

            Tm = tmp[:,0]
            Em = tmp[:,1]

            # plot the results
            plt.errorbar(tt[tt<9], hv[tt<9], yerr=hv_err[tt<9], 
                fmt=" ", color="red", linewidth=2)
            plt.semilogx(tt[tt<9],hv[tt<9],
                linestyle='-' ,color="red", label="Observed")

            plt.semilogx(Tm[Tm<9], Em[Tm<9], color="blue",
                linestyle='-', linewidth=2, label="litho1.0")
            plt.plot(tt[tt<9], hv[tt<9],'.' ,color="r", 
                    markersize=9, markeredgecolor='r')

            plt.xlabel("Period (s)", fontsize=20)
            plt.ylabel("log(H/V)", fontsize=20)
            plt.legend(fontsize=16, scatterpoints=1,loc='lower right')
            plt.ylim(-1,1)
            xTicks = np.arange(2,11,2)
            xTicks[0]=2
            xTickMark = ["%s" % x for x in xTicks]
            plt.xticks(xTicks, xTickMark)
            plt.xlim(1.9,10)
            plt.grid()
            
            mn=month_name[int(month)]
            fig.suptitle("%s %s - Angles %s$^\circ$\n Model order %d" 
                    %(mn, year, angTstr,order), size=22)

    plt.show()

#---------------------------------------------------------------------
if __name__=="__main__":
    main()





