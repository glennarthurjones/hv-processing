#!/usr/bin/env python 
#
# Python module for the pre-processing of continuous seismic data for noise 
# analysis. 
#
# The modules:
#       make_same_length
#       check_and_phase_shift
#        
# are taken from MSNoise preprocessing routines and modified accordingly. 
# 
# [1] Lecocq T., Caudron, et F. Brenguier (2014), MSNoise, a Python Package
# for Monitoring Seismic Velocity Changes Using AMbiend Seismic Noise, 
# Seismological Research Letters, 85(3), 715-726
# 
# Modified 2018-08-20 by Glenn Jones, Swansea University, UCL
#            + Allow user defined taper length check_and_phase_shift
#
# Modified 2019-05-31 by Glenn Jones, Swansea University, UCL
#           + Use of obspy.signal.rotate.rot2ZNE to rotate traces 
#
# Modified 2019-07-17 by Glenn Jone, Swansea University, UCL
#           + Added function whiten to whiten the amplitude spectrum of traces
#

import numpy as np
import scipy.fftpack
import logging
from obspy.signal.rotate import rotate2zne
#==============================================================================
def make_same_length(st):
    """
    This function takes a stream of equal sampling rate and makes sure that all
    channels have the same length and the same gaps.
    """

    # Merge traces
    st.merge()

    # Initialize arrays to be filled with start+endtimes of all traces
    starttimes = []
    endtimes = []

    # Loop over all traces of the stream
    for tr in st:
        # Force conversion to masked arrays
        if not np.ma.count_masked(tr.data):
            tr.data = np.ma.array(tr.data, mask=False)
        # Read out start+endtimes of traces to trim
        starttimes.append(tr.stats.starttime)
        endtimes.append(tr.stats.endtime)

    # trim stream to common starttimes
    st.trim(max(starttimes), min(endtimes))

    # get the mask of all traces, i.e. the parts where at least one trace has
    # a gap
    if len(st) == 2:
        mask = np.logical_or(st[0].data.mask, st[1].data.mask)
    elif len(st) == 3:
        mask = np.logical_or.reduce((st[0].data.mask, st[1].data.mask, st[2].data.mask))
    elif len(st)==1:
        return st.split()

    # apply the mask to all traces
    for tr in st:
        tr.data.mask = mask
    
    # remove the masks from the stream 
    st = st.split()
    return st

#==============================================================================
def check_and_phase_shift(trace, taper_length=20.0):
    # print trace
    trtype = trace.data.dtype
    if trace.stats.npts < 4 * taper_length*trace.stats.sampling_rate:
        trace.data = np.zeros(trace.stats.npts)
        return trace

    dt = np.mod(trace.stats.starttime.datetime.microsecond*1.0e-6,
                trace.stats.delta)
    if (trace.stats.delta - dt) <= np.finfo(float).eps:
        dt = 0.
    if dt != 0.:
        if dt <= (trace.stats.delta / 2.):
            dt = -dt
#            direction = "left"
        else:
            dt = (trace.stats.delta - dt)
#            direction = "right"
        logging.debug("correcting time by %.6fs"%dt)
        trace.detrend(type="demean")
        trace.detrend(type="simple")
        trace.taper(max_percentage=None, max_length=1.0)

       # n = next_fast_len(int(trace.stats.npts))
	n = 1	
	while n < int(trace.stats.npts): n *=2
        
	FFTdata = scipy.fftpack.fft(trace.data, n=n)
        fftfreq = scipy.fftpack.fftfreq(n, d=trace.stats.delta)
        FFTdata = FFTdata * np.exp(1j * 2. * np.pi * fftfreq * dt)
        FFTdata = FFTdata.astype(np.complex64)
        scipy.fftpack.ifft(FFTdata, n=n, overwrite_x=True)
        trace.data = np.real(FFTdata[:len(trace.data)]).astype(np.float)
        trace.stats.starttime += dt
        del FFTdata, fftfreq
        #clean_scipy_cache()
        trace.data = trace.data.astype(trtype)
	return trace
    else:
	return trace

#==============================================================================
def nextpow2(npts):
    """
    Compute the number of samples in next power of 2 npts

    Input:
        npts: number of points in the seismic trace

    Outputs:
        np2: next power of 2 samples from npts
    """
    if npts > 0:
        np2 = pow(2, np.ceil(np.log(npts)/np.log(2)))
    else:
        np2=0

    return int(np2)

#==============================================================================
def gapCheck(st, max_gap=10):
    """
    Check for gaps in the data stream and interpolate

    Input:
        st: obspy trace stream
        max_gap: maximim size of the gap to be interpolated

    Output:
        st: interpolated obspy trace stream
    """
    if len(st.get_gaps())> 0:
       	only_too_long = False
	# loop over the gaps and check to see if we can interpolate over some of them
	while st.get_gaps() and not only_too_long:
	    too_long = 0
	    gaps = st.get_gaps()
	    # loop over the gaps and see if they are larger than max_gap
	    for gap in gaps:
		# interpolate and remove the current gap
		if int(gap[-1]) <= max_gap:
                    # get the stream index which corresponds to this gap
                    for kk, tr in enumerate(st):
                        if tr.stats.channel==gap[3]:
                            if tr.stats.endtime==gap[4]:
                                idxE = kk
                            elif tr.stats.starttime==gap[5]:
                                idxS = kk
                    try:
		        st[idxE] = st[idxE].__add__(st[idxS], method=1,
                                fill_value="interpolate")
                        st.remove(st[idxS])
                    except:
		       st.remove(st[idxS])
		    break
		else:
		    too_long += 1
                 
	    if too_long == len(gaps):
		only_too_long = True

    return st

#==============================================================================
def nanCheck(st):
    '''
    Check to see if the traces contain Nans or Infs

    Input:
        st : obspy trace stream 

    Output:
        errorStr  : output flag and name of the trace with errors 
    '''
    errorStr = []
    for tr in st:
        if np.isnan(tr.data).any():
            erS = 'Nan detected in file %s' %fl
            errorStr.append(erS)
        if np.isinf(tr.data).any():
            erS = 'Inf detected in file %s' %fl
            errorStr.append(erS)
    
    return errorStr

#==============================================================================
def traceRot(st, inv):
    '''
    Uses obspy.signal.rotate.rotate2ZNE to rotate data. Assumes that we have
    3 traces and 3 inventory items which correspont to the trace names

    Input:
        st  :   obspy trace stream
        inv :   obspy inventory file with the appropriate header information
        err :   any error messages to be 
    Output:
        stR :   obspy trace stream of the rotated data
    '''
    
    stR = st.copy()
    # storage arrays
    traces = np.zeros((st[0].stats.npts,3))
    azimuth = []
    dip = []

    # loop over the 
    for j, tr in enumerate(st):
        traces[:,j] = tr.data
        ad_dic = inv.get_orientation(tr.get_id())        
        azimuth.append(ad_dic['azimuth'])
        dip.append(ad_dic['dip'])  

    # attempt the rotation
    [Z,N,E] = rotate2zne(traces[:,0], azimuth[0], dip[0],
        traces[:,1], azimuth[1], dip[1],
        traces[:,2], azimuth[2], dip[2])
    
    Rdta={'Z':Z, 'N':N, 'E':E}
    # allocate the data and rename components of the data stream
    for j,comp in enumerate(['Z','N','E']):
        # rename the components
        stR[j].stats.channel = stR[j].stats.channel[:-1] + \
                comp
        # update the sac header with the new channel name
        stR[j].stats.sac['kcmpnm']=stR[j].stats.channel
        # update the data 
        stR[j].data = Rdta[comp]
        
    return stR

#==============================================================================
def whiten(tr,freq=[]): 
    '''
    Function to generate a flat Fourier spectrum for a given obspy
    data stream which is not originally white. The range of whitening
    is either from 0Hz to Nyquist or for a user defined frequency band.
    Whitening tends to sharpen both signal and noise and is often applied
    in ambient noise studies.

    Input:
        tr      :   obspy trace stream
        freq    :   frequency range to apply the whitening. 
                    Default is all
    Output:
        trW     :   whitened trace stream

    This function is based on the matlab function Spectral Whitening 
    written by Dr Erol Kalkan P. E. (ekalkan@usgs.gov) 
    and the function whiten from MSNoise [1]  

    [1] Lecocq T., Caudron, et F. Brenguier (2014), MSNoise, a Python Package
    for Monitoring Seismic Velocity Changes Using AMbiend Seismic Noise, 
    Seismological Research Letters, 85(3), 715-726

    '''
    trW = tr.copy()
    # get the next power of 2 of the trace and generate the f-space
    npts = nextpow2(len(tr))
    # get data sampling rate in Hz and set nyquist frequency
    delta = tr.stats.sampling_rate
    nyquist = delta/2.

    # check to see if we have defined a frequency band to whiten
    # the signal
    if not freq: 
        freq = [0, nyquist]

#    # get the frequency interval for our f-space 
#    df = nyquits/(npts/2) 
#    fspace = np.arange(-npts/2, npts/2)*df

    # get the frequency axis using scipy this doesn't require me to use
    # fftshift during the transform and I can use it as is
    # We throw away the negative frequencies since our ouptut is a real 
    # function we can just add the symmetric part of the trace
    fvect = scipy.fftpack.fftfreq(npts, d=tr.stats.delta)[:npts // 2]
    # taper the trace before taking the FFT to avoid spectoral leakage
    tr.taper(max_percentage=0.05)
    dataF = scipy.fftpack.fft(tr.data, n=npts)
    # get the phase of the FFT 
    phase = np.angle(dataF)

    # get the indices of fft which is of interest to us
    idx = np.where((fvect >= min(freq)) & (fvect <= max(freq)))[0]
    # set up a cosine taper around the frequencies of interest
    iPad = 100 # number of samples used to generate the taper
    low = idx[0] - iPad
    high = idx[-1] + iPad
    # check to see if the index are out of range
    if low < 0:
        low = 0
    if high > (npts/2):
        high = int(npts // 2)
    # set up the tapers
    lTaper = np.cos(np.linspace(np.pi/2., np.pi, idx[0] - low))**2
    rTaper = np.cos(np.linspace(0, np.pi/2., high - idx[-1]-1))**2

    # set values outside the taper equal to 0
    dataF[0:low] *= 0
    dataF[low:idx[0]] = lTaper*np.exp(1j *phase[low:idx[0]])
    # set amplitudes in idx equal to 1
    dataF[idx] = np.exp(1j * phase[idx])
    if not idx[-1]==npts:
        dataF[idx[-1]+1:high] = rTaper*np.exp(1j *phase[idx[-1]+1:high])
        dataF[high: npts+1] *= 0

    # Since the output signal is real the FFT is symmetric around 0 frequency
    # so we mirror the signal into -ve frequencies with the phase flipped
    dataF[(npts //2)+1:] = dataF[1:(npts //2)].conjugate()[::-1] 

    # compute the IFFT 
    dataW = scipy.fftpack.ifft(dataF, n=npts)

    return np.real(dataW), dataF 
    


