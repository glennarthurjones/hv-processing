#!/usr/bin/env python
# Python script to plot the H/V results from 3 earthquakes in July 2012
# The H/V results look a bit odd with strong N-S and E-W components. 
# The goal of this script is to workout if the polarization results of 
# the processing scripts are ok?

import os 
import sys 
import glob
import numpy as np
import matplotlib.pyplot as plt
import datetime
from mpl_toolkits.basemap import Basemap
from obspy import read_inventory, read, UTCDateTime
from obspy.geodetics.base import gps2dist_azimuth
from obspy.imaging.beachball import beach
from matplotlib import colors
#---------------------------------------------------------------------
def flipBaz(azi):
    '''
    Flip the back azimuth values to be between 0-360 
    '''
    baz = azi
    for jj, theta in enumerate(baz):
        if theta < 0:
            baz[jj] = 180 + theta % 180
            
    return baz 

#---------------------------------------------------------------------
# load the trace data and trim appropriatley 
def getTraces(fname, locID, t0, tE):
    '''
    Use obspy to read the traces and do some processing 
    '''
    dtaFolder = "%s/processed/*.%s*BHZ*.sac" %(fname, locID)
    st = read(dtaFolder)
    st += read(dtaFolder.replace("BHZ", "BHN"))
    st += read(dtaFolder.replace("BHZ", "BHE"))
    st = st.merge()
    st = st.sort()
    # trim the traces 
    st.trim(t0, tE, nearest_sample=False)

    return st

#---------------------------------------------------------------------
def main():
    # Define variables and the Earthquake information for the data
    station = "DY2G"
    year = "2012"
    months = "07"
    month = int(months)
    days = [25, 26, 28]
    measurement_code = "pol_mp"
    dop_thresh = 0.9
    day_sec = 24*60*60
    loc = '02' 
    # Information taken from CMT
    EQlons = np.array([159.71, 65.88, 153.07])
    EQlats = np.array([-9.84, -17.47, -4.85])
    EQtimes = ["11:20:35", "05:33:48", "20:04:4"]
    # info from CMT
    EQmag = [6.5, 6.7, 6,5] # surface wave mag
    MT = np.array([[0.162, 0.188, -0.350, 5.600, -0.554, 1.680],
        [-0.094, -0.904, 0.998, -0.147, 0.016, 0.445],
        [0.598, -0.698, 0.099, 0.330, 0.176, 0.133]])
   
    traceCut = 3 # length of time series we wish to look at

    for kk, day in enumerate(days):
        day_folder = "%s/%s.%s/%02d" %(station, year, months, day)
        fname = ("%s/%s.%02d.%s.%s.%s.asc" 
            % (day_folder, year, month, day, measurement_code, loc))

        azi, freq, freq_list, dop, time, hv =[],[],[],[],[],[]
        if not fname:
            continue

        with open(fname) as fp:
            for line in fp:
                if line.split()[0]== year:
		    pass
		else:
		    azi_tmp = float(line.split()[0])
		    freq_tmp = float(line.split()[1])
		    dop_tmp = float(line.split()[2])
		    time_tmp = float(line.split()[3])
		    hv_tmp = np.log10(float(line.split()[5]))
					
		    # check to make sure the results are within our DOP
		    if dop_tmp >= dop_thresh:
		        azi.append(azi_tmp)
			freq.append(freq_tmp)
			dop.append(dop_tmp)
			time.append(time_tmp)
			hv.append(hv_tmp)

			# check to see if there are unique frequency values
			if freq_tmp not in freq_list:
			    freq_list.append(freq_tmp)

        # compute the period  
        T = []
        for f in freq:
	    t=1./f #compute the period
            T.append(t)

        # compute the EQ time in seconds
        [hh,mm,ss] = EQtimes[kk].split(":")
        EQtimeSec = datetime.timedelta(hours=int(hh), minutes=int(mm), seconds=int(ss)).total_seconds()
        # compute 2 hours after the earthquake in seconds
        EQtimeSecA = EQtimeSec + traceCut*60*60

        # filter the data based on the earthquake interval times
        T = np.array(T)
        freq = np.array(freq)
        time = np.array(time) 
        hv = np.array(hv)
        baz = np.array(azi)
        dop = np.array(dop)

        TF = T[(time >= EQtimeSec) & ( time < EQtimeSecA)]
        freqF = freq[(time >= EQtimeSec) & ( time < EQtimeSecA)]
        timeF = time[(time >= EQtimeSec) & ( time < EQtimeSecA)]
        hvF = hv[(time >= EQtimeSec) & ( time < EQtimeSecA)]
        bazF = baz[(time >= EQtimeSec) & ( time < EQtimeSecA)]
        dopF = dop[(time >= EQtimeSec) & ( time < EQtimeSecA)]

        fig=plt.figure(kk, figsize=(12., 9) )
        plt.rcParams['xtick.labelsize']=14
        plt.rcParams['ytick.labelsize']=14
    
#        plt.subplot(2,2,1)
        plt.subplot2grid((6,2),(0,0), rowspan=3)
        # load the station meta data 
        staName = glob.glob("%s/resp/STXML*%s.BHZ" %(day_folder, loc))[0]
        staDta = read_inventory(staName)
        lats=staDta[0][0].latitude
        lons=staDta[0][0].longitude
    
        m= Basemap(projection='aeqd', lat_0 = lats, lon_0 = lons, round=True)
        m.shadedrelief(scale=0.1)
    
        x, y =  m(lons, lats)
        m.scatter(x,y,200,color="r",marker="v",edgecolor="k",zorder=3)
    
        x, y = m(EQlons[kk], EQlats[kk]) 
        # plot the great circles
        m.drawgreatcircle(lons,lats,EQlons[kk],EQlats[kk],
            linewidth=2,color='k',alpha=0.4)
        ax =plt.gca()
        b = beach(MT[kk,:], xy=(x,y), width=2000000, linewidth=1.5)
        #b.set_zorder(10)
        ax.add_collection(b)
        #=========================================
        # Polar histogram plot
#        axP = plt.subplot(2,2,2, polar=True)
        axP = plt.subplot2grid((6,2), (0, 1), rowspan=3, polar=True)
        # setting the axis to be in geographical coordinates
        axP.set_theta_direction(-1)
        axP.set_theta_offset(np.pi/2.0)
        # generate a polar histogram of the back azimuth as a function of frequency
        deltaF = 0.01    
        fBins = np.linspace(min(freq_list), max(freq_list), 10)
        deltaA = np.deg2rad(5)
        aBins = np.arange(-np.pi, np.pi+deltaA, deltaA) # angular bins
        thetaBin, rBin = np.meshgrid(aBins, fBins)
        bazR = np.deg2rad(bazF)

        # compute the 2D histogram 
        [bazFreqF, xbin, ybin] = np.histogram2d(x=freqF, y=bazR, 
                bins=[fBins, aBins])
        # normalize the plot based on the maximum  
        bazFreqMax = bazFreqF.max()
        bazFreqMin = bazFreqF.min()
        bazFreqN = (bazFreqF -bazFreqMin) / (bazFreqMax - bazFreqMin)  
        # plot the results
        normC = colors.Normalize(vmin=bazFreqMin, vmax=bazFreqMax)
    
        pc=axP.pcolor(thetaBin, rBin, bazFreqN, 
                edgecolors='w', linewidths=0.25,
                cmap='Spectral_r')
        # mask values below the minimum we have 
        axP.set_ylim([0, 0.5])
        #set the angular tick labels
        thetaticks = np.arange(0,360,45)
        axP.set_thetagrids(thetaticks, frac=1.15) 
        cbar = plt.colorbar(pc,pad=0.11, fraction=0.046)
        cbar.set_label('Normalised count', fontsize=16)
        # remove the radial tick labels
        axP.set_yticklabels([])
        
        # get the 
        [gc_dist, gc_baz, gc_az] = gps2dist_azimuth(lats, lons, 
                EQlats[kk], EQlons[kk])

        plt.plot(np.deg2rad([gc_baz, gc_baz]), [min(freq_list), max(freq_list)],
                '-k', color='#D3D3D3', linewidth=1.5)

        #=========================================
        # scatter plot of frequency v time coloured by baz
        bazFF = flipBaz(bazF)
#        plt.subplot(2,2,3)
        plt.subplot2grid((6,2), (3,0), rowspan=3)
        timeN = timeF - EQtimeSec
        plt.scatter(freqF, timeN, 20, c = bazFF, marker=".", cmap="jet",
                edgecolor="face", vmin=0, vmax=360)
        cb = plt.colorbar()
        cb.ax.set_title("BAZ ($^\circ$)",size=16)
        
        plt.xlim([min(freq_list), max(freq_list)])
        plt.ylim([0, traceCut*60*60])

        timeTick = np.arange(0,traceCut + 0.1,0.5).tolist()
        timeTickMark = ["%s" % (x*60) for x in timeTick]
        timeTick = np.multiply(timeTick,60.*60.).tolist() # convert to seconds 
    
        plt.yticks(timeTick,timeTickMark)
        plt.ylabel("Time (seconds)",size=16)
        plt.xlabel("Frequency (Hz)", size=16)
        plt.grid()

        #=========================================
        # plot the traces
        t0 = UTCDateTime("%s-%s-%s %s" % 
                (int(year), int(month), int(day), EQtimes[kk]))
        tE = t0 + traceCut*60*60
        st = getTraces(day_folder, loc, t0, tE)
        for q, tr in enumerate(st):
            comp = tr.stats.channel
            plt.subplot2grid((6,2), (q+3,1))

            trTime = np.arange(0, tr.stats.npts, 1)*tr.stats.delta
            plt.plot(trTime, tr.data, '-k', linewidth=2)

            # set the y axis to have only 3 ticks 
            yT = np.array([-max(abs(plt.yticks()[0])), 0,max(abs(plt.yticks()[0]))])
            yTMark = ["%1.0e" %x for x in yT] 
            plt.yticks(yT, yTMark)
            if q ==1:
                plt.ylabel('Displacement (m)', size=16)
            
            if q == 2:
                # setting up the tick labels and markers
                timeTick = np.arange(0,traceCut + 0.1,0.5).tolist()
                timeTickMark = ["%s" % (x*60) for x in timeTick]
                timeTick = np.multiply(timeTick,60.*60.).tolist() # convert to seconds 
                plt.xticks(timeTick,timeTickMark)
                plt.xlabel('Time (seconds)', size=16)
            else: 
                timeTick = np.arange(0,traceCut + 0.1,0.5).tolist()
                timeTickMark = ["" for x in timeTick]
                timeTick = np.multiply(timeTick,60.*60.).tolist() # convert to seconds 
                plt.xticks(timeTick,timeTickMark)
          
            plt.xlim([0, traceCut*60.*60.])
            plt.legend(['%s' %comp], loc='upper right', framealpha=0.7)
            plt.grid()
        
        plt.subplots_adjust(left=0.1, bottom=0.07, right=0.92, 
                wspace=0.30, hspace=0.4)
        fig.suptitle("%s-%02d-%02d %s\n Ms = %1.1f, BAZ = %1.1f$^\circ$" 
                %(year, int(month), int(day), EQtimes[kk], EQmag[kk], gc_baz), size=22)

        plt.show()

        
#---------------------------------------------------------------------
if __name__=="__main__":
    main()

