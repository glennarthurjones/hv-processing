#!/usr/bin/env python
#
# Python script to make a copy of the DY2G station at location 01
# and rename them based on flipping the traces with components 1 2 
# to E, N. Unfortunatley this cannot be a simple renaming of files
# since the E component data must be multiplied by -1

import numpy as np
import os 
import glob 
from obspy.core import read, UTCDateTime, Stream
from calendar import monthrange
import re

def sort_nicely(l):
    """ Sort the given list in the way that humans expect.
    """
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    l.sort( key=alphanum_key )

def traceFlip(st):
    """
        Re-order the traces. 1 --> E, 2 --> N 
        Returns a new trace stream with E and N components
    """
    # sort out the N and E components
    n = st.select(component='2')
    n.stats.channel = n.stats.channel.replace('2', 'N')

    e = st.select(component='1')
    e.stats.channel = n.stats.channel.replace('1', 'E')
    e.data = -1. * e.data

    stNE = Stream(traces=[n ,e]) 
    return stNE

def saveTraces(st, fl):
    """
        Rename the traces and save
    """
    dirname = os.path.dirname(fl)
    basename = os.path.basename(fl)
    baseChan = basename.split('_')[0].split('.')[-1]
    for tr in st:
        chan = tr.stats.channel
        fname = basename.replace(baseChan, chan)
        fout = dirname + fname
        tr.write(fout, format='SAC')
        


# set up some variables
station_folder=['DY2G']
years = ["2012"]
loc = '01'
months = ['1','2','3']  

for jj , data_folder in enumerate(station_folder):
    for year in years:
        for month in months:
            base_dir = data_folder + "/%s.%02d" %(year, int(month))
    	    nDays = monthrange(int(year), int(month))[1]
            days = ['%02d' % (jj+1) for jj in range(0,nDays)]
            for day in days:
                dirname = "%s/%s" % (base_dir, day)
    	        file_list = glob.glob("%s/processed/*.%s.*Z*.sac"
                            % (dirname, loc))
                sort_nicely(file_list)
                for fl in file_list:
                    # load the horizontal traces
                    st = read(fl.replace('Z', '1'))
                    st += read(fl.replace('Z', '2'))

                    # save the traces
                    saveTraces(st, fl)


