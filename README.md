# Polarisation analysis

Series of python script to download, pre-process and process seismic ambient  noise data to obtain their polarisation and H/V properties

# Dependencies

**Python libraries**
+ numpy
+ scipy
+ matplotlib
+ obspy
+ [basemap](https://matplotlib.org/basemap/)
+ netCDF4
+ Pandas

**Stand alone code**
+ [Seismic Analysis Code (SAC) version 101.6a](http://ds.iris.edu/ds/nodes/dmc/manuals/sac/)
+ [obspyDMT](https://github.com/kasra-hosseini/obspyDMT)
+ [Time-Frequency Dependent Polarization](http://diapiro.ictja.csic.es/gt/mschi/SCIENCE/tseries.html#software)
+ [Sea pressure data](https://www.esrl.noaa.gov/psd/data/composites/day/)

# Example processing flow for ROSA station from the Azores

Download the data.

```
python getSeismicdata.py --station ROSA --years 2022 --months 1 2 3 4 --loc " " --channels 'B*' --network PM --data_source "http://ceida.ipma.pt"
```

Preprocessing the data using a combination of obspy and SAC macros.
It is important that you include a SAC macro called remove_resp is located in $SACHOME/sac/aux/macros. An example remove_resp macro is given.

```
python preprocessData.py --station ROSA --years 2022  --months 1 2 3 4  --locList "" --freqFilt 0.01 1.1 --rot=True --subsamp 4
```

QC the data before processing. The code will generate a GUI which can then be used to flag days where the data may have issues and will generate a file called exclude.txt. This file can then be used during the main H/V processing to exclude any poor data.

```
python dataQC.py --station ROSA --year 2022 --months 1 2 3 4 --locID ''
```

If we are unhappy with any of the preprocessing we can cleanup using the follwing command. The user will be prompted by the scrip to enter the station name, year and months to be removed.

```
python cleanupHVsac.py
```

Process the data using Time-Frequency Dependent Polarization analysis. The --exclude option allows for an exclude file to be included. The --nodes option allows for the processing to be divided accross a number of different nodes if the user has a multi-core processror. Care should be taken wth the --nodes option to makes sure the user has sufficient memory to load the data accross multiple nodes.

```
python measure_HV.py --station ROSA --years 2022 --months 1 2 3 4 --code pol_az --exclude "xclude..txt" --nodes 6 
```

Check to processing logs to see if we have any errors.

```
python logError_HV.py --station ROSA --year 2022 --months 1 2 3 4--code pol_az --locID ''
```

Merge the results into monthly and yearly summaries. Note that the yearly summary is  dependednt on having the monthy examples being completed. 

```
python ResultsSummaryEllip.py --station ROSA --years 2022 --months 1 2 3 4 --code pol_az --locID "" --dop 0.95
python ResultsSummaryEllipYear.py --stations ROSA --years 2022 --code pol_az --locID "" --dop 0.95
```

Summary plots of the polarisation results. 

```
plotPolMonth.py --station ROSA --year 2022 --months 1 2 3 4 --code pol_az --locID "" --fmin  0 --fmax 1 --dop 0.95
plotPolMonthAbs.py --station ROSA --year 2022 --months 1 2 3 4 --code pol_az --locID "" --fmin 0 --fmax 1 --dop 0.95
plotHVmonth.py --station ROSA --year 2022 --months 1 2 3 4 --code pol_az --locID "" --maxT 20 --minT 1 --dop 0.95
plotStatMonth.py --station ROSA --year 2022 --months 1 2 3 4 --code pol_az --locID "" --dop 0.95

```









