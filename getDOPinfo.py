#!/usr/bin/env python
#
# Module to read the DOP files from a list of file names and output 
# arrays of the collated information from the files

import os
from numpy import array

def readDOP(filename):
    '''
    Read the ellipticity output files for a given set of input files

    Parameters
    Input:
        filename    :   list of filenames we wish to collate the DOP data

    Output:
        azi         :   numpy array of the azimuths of the measurements 
        freq        :   numpy array of frequencies of the measurements
        freq_list   :   numpy array of unique frequency values
        dop         :   numpy array of the degree of polarizations
        time        :   numpy array of times
        hv          :   numpy array of horizontal vs vertical measurements
    '''
    azi, freq, freq_list, dop, time, hv =[],[],[],[],[],[]
   
    if not filename:
        print('WARNING: No files named %s found' % filename)
        return 

    # loop over the files 
    for fl in filename:
        flB = os.path.basename(fl)
        locID= flB.split('.')[4]
        with open(fl) as fp:
            for line in fp:
                if line.split()[0]== year:
		    pass
		else:
		    azi_tmp = float(line.split()[0])
		    freq_tmp = float(line.split()[1])
		    dop_tmp = float(line.split()[2])
		    time_tmp = float(line.split()[3])
		    hv_tmp = np.log10(float(line.split()[5]))
					
		    # check to make sure the results are within our DOP
		    if dop_tmp >= dop_thresh:
		        azi.append(azi_tmp)
			freq.append(freq_tmp)
			dop.append(dop_tmp)
			time.append(time_tmp)
			hv.append(hv_tmp)

			# check to see if there are unique frequency values
			if freq_tmp not in freq_list:
			    freq_list.append(freq_tmp)

    # compute the period  
    T = []
    for f in freq:
	t=1./f #compute the period
        T.append(t)
  
    # convert T, time and hv into numpy arrays
    T = np.array(T)
    freq = np.array(freq)
    time = np.array(time) 
    hv = np.array(hv)
    baz = np.array(azi)
    dop = np.array(dop)


