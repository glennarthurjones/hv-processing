#!/usr/bin/env python
#
# Python script to take the Ellipticity measurements for a given
# month and azimuth and generate histogram of the distribution of 
# observations
#

import sys
import argparse
from calendar import month_name
from numpy import array, mean, std, median, percentile, unique, \
        transpose, loadtxt, append
import matplotlib.pyplot as plt
from matplotlib import colors
#---------------------------------------------------------------------
def plotHist(freq, hv, station, year, month, angles, dop):
    '''
    Function to generate the histogram to be plotted
    '''

    if angles == None:
        angMin=0
        angMax=360
    else:
        if angles[0] < 0:
            angMin = 180 + angles[0] % 180    
        else:
            angMin = angles[0]

        if angles[1] < 0:
            angMax = 180 + angles[1] % 180    
        else:
            angMax = angles[1]

    # filter the data based on a minimum frequency - T=9s
    fmin = 1/9.
    f = freq[freq>=fmin]
    
    # get some basic statistics about the distrbution
    mu = mean(freq)
    sigma = std(freq)

    # sort out the frequency bins so that they are centred on the freqList
    freqList = unique(f)
    df = freqList[1] - freqList[0]
    fbins = freqList-df/2
    fbins = append(fbins,fbins[-1]+df/2)

    # plot the results
    fig = plt.figure(int(month), figsize=(10,9))
    plt.rcParams['xtick.labelsize']=16
    plt.rcParams['ytick.labelsize']=16
    plt.hist(f, fbins, facecolor='blue', alpha=0.75) 

    plt.ylabel('Count', fontsize=20)
    plt.xlabel('Frequency (Hz)', fontsize=20)

    mn =month_name[int(month)]
    plt.title("%s - %s %s \n Angles = %d-%d - DOP = %s" 
            %(station ,mn, year,angMin , angMax , dop), fontsize=22)
    plt.grid()
    # add statistics text to the plot
    yv = plt.gca().get_ylim()[1]*0.9
    lb = "$\mu$=%.2f  $\sigma$=%.2f" %(mu, sigma)
    plt.text(0.3, yv, lb, fontsize=20)

#---------------------------------------------------------------------
def aveHV(freq_list, freq, hv, minObs, maxT):
    '''
    Estimate the average properties of the HV measurements as a 
    function of period
    '''
    hv_mean, hv_std, hv_median, hv_err_pc, T_list \
            = [], [], [], [], []
    for f in sorted(freq_list, reverse=True):
        T = 1./f # compute the period
	idx=[i for i,val in enumerate(freq) if val==f]
	idx=array(idx)
        # check to make sure we have enough observations 
        # to compute the statistsics
        if len(idx) > minObs: 
            # now do the calculations	
            hv_sub = array(hv)[idx]
            hv_mean_tmp = mean(hv_sub)
            hv_std_tmp = std(hv_sub)
            hv_median_tmp = median(hv_sub)
            q1, q2 = percentile(hv_sub,[84.1, 15.9])
            err_inf = abs(q2 - hv_median_tmp)
    	    err_sup = abs(q1 - hv_median_tmp)
    	    iqr_err = (err_inf + err_sup)/2.0
            # check to see if we have a maximum period filter
            if not maxT:
                hv_mean.append(hv_mean_tmp)
    	        hv_std.append(hv_std_tmp)
    	        hv_median.append(hv_median_tmp)
    	        hv_err_pc.append(iqr_err)
    	        T_list.append(T)
            elif T <= maxT:
                hv_mean.append(hv_mean_tmp)
    	        hv_std.append(hv_std_tmp)
    	        hv_median.append(hv_median_tmp)
    	        hv_err_pc.append(iqr_err)
    	        T_list.append(T)

    return T_list, hv_mean, hv_std, hv_median, hv_err_pc

#---------------------------------------------------------------------
def flipBaz(azi):
    '''
    Flip the back azimuth values to be between 0-360 
    '''
    baz = azi
    for jj, theta in enumerate(baz):
        if theta < 0:
            baz[jj] = 180 + theta % 180
            
    return baz 

#---------------------------------------------------------------------
def angularFilter(freq, hv, azi, ang_thresh):
    '''
    Filter the data based on the user defined angular threshold
    '''
    baz = flipBaz(azi)
    if ang_thresh == None:
        return freq, hv, azi
    else:
        if ang_thresh[0] < 0:
            angMin = 180 + ang_thresh[0] % 180    
        else:
            angMin = ang_thresh[0]

        if ang_thresh[1] < 0:
            angMax = 180 + ang_thresh[1] % 180    
        else:
            angMax = ang_thresh[1]
        # filter the frequency and H/V values
        if angMin > angMax:
            idx = [i for i, v in enumerate(baz) if v >= angMin or v < angMax]
        else:
            idx = [i for i, v in enumerate(baz) if v >= angMin and v < angMax]

        freqF = freq[idx]
        hvF = hv[idx]
        aziF = azi[idx]

        return freqF, hvF, aziF

#---------------------------------------------------------------------
def main():
    '''
    Script to take the polarization results from script measure.py
    and plot a histogram of the number of observations as a function of month
    '''

    parser = argparse.ArgumentParser(description=
                        "Compute the statistical H/V ratio for a months worth of data",
                        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--station', type=str,
                    help='station name to process', required=True)
    parser.add_argument('--year', nargs=1,
                    help='year to process', required=True )
    parser.add_argument('--months', nargs='+',
                    help='months to process', required=True)
    parser.add_argument('--code', type=str,
                    help='measurement base code', 
                    required=True)
    parser.add_argument('--locID', nargs='+' ,
                    help='location ID of the station', 
                    required=True)
    parser.add_argument('--dop', type=float,
                    help='DOP filter. Default 0.9', required=False)
    parser.add_argument('--angles', nargs=2, type=float,
                    help='Optional angular filter to compute H/V stats.\n'
                    'The input should ordered minAng max Ang.Default=0-360', 
                    required=False)

    # setting up the variables
    args = vars(parser.parse_args())
    station = args['station']
    year = args['year'][0]
    months = args['months'] 
    if len(months) > 1:
        months.sort(key=int)

    measurement_code = args['code']
    locID = args['locID']
    if args['dop'] is None:
        dop_thresh = 0.9
    else:
        dop_thresh = args['dop']

    if args['angles'] is None:
        ang_thresh = None 
    else:
        ang_thresh = args['angles']

    day_sec = 24.*60.*60.
    # loop over the months
    for kk, month in enumerate(months):
        dirname = "%s/%s.%02d" %(station, year, int(month))
        for loc in locID: 
            # load the relevant summary file
            fnameIn = ('%s/%s_%s_summary.txt'
                    %(dirname, measurement_code, loc))
            try:
                azi, freq, dop, doy, time, hv = loadtxt(fnameIn,
                        delimiter=',', comments='#', unpack=True)      
            except Exception as error:
                print('WARNING: %s' %error)
                continue

            print '--> Processing file %s' %fnameIn
            freq_list = list(unique(freq))
            # compute the period 
            T = []
            T_list = []
            for f in freq:
	        t=1./f #compute the period
                T.append(t)
            
            # filter the frequency and hv results based on dop
            freqD = freq[dop >= dop_thresh]
            hvD = hv[dop >= dop_thresh]
            aziD = azi[dop >= dop_thresh]
            # filter the data based on the angle of interest
            freqA, hvA, aziA = angularFilter(freqD, hvD, aziD, ang_thresh) 

            # histogram plot of the data
            plotHist(freqA, hvA, station, year, month , ang_thresh, dop_thresh)


    plt.show()

#---------------------------------------------------------------------
if __name__=="__main__":
    main()

